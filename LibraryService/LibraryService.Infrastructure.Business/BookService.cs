#region

using System.Linq;
using System.Threading.Tasks;
using LibraryService.Domain.Core;
using LibraryService.Infrastructure.Data.Repositories;
using LibraryService.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

#endregion

namespace LibraryService.Infrastructure.Business
{
    public class BookService : IBookService
    {
        private readonly BookRepo _bookRepo;
        private readonly BookInstanceRepo _bookInstanceRepo;

        public BookService(BookInstanceRepo bookInstanceRepo, BookRepo bookRepo)
        {
            _bookInstanceRepo = bookInstanceRepo;
            _bookRepo = bookRepo;
        }

        public async Task CreateBook(Book book)
        {
            book = await _bookRepo.AddAsync(book);
            var instance = new BookInstance(book);
            await _bookInstanceRepo.AddAsync(instance);
        }

        public async Task AddBookInstance(Book book)
        {
            if (book.Id <= 0)
            {
                await CreateBook(book);
            }
            else
            {
                var instance = new BookInstance(book);
                await _bookInstanceRepo.AddAsync(instance);
            }
        }

        public async Task<bool> DeleteBook(Book book)
        {
            var instances = await _bookInstanceRepo.GetListQuery().Where(p => p.IdBook == book.Id).ToListAsync();
            var result = _bookInstanceRepo.DeleteRange(instances);
            return result && _bookRepo.Delete(book);
        }
    }
}