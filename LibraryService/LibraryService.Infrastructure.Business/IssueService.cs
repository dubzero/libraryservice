#region

using System;
using System.Linq;
using System.Threading.Tasks;
using LibraryService.Domain.Core;
using LibraryService.Domain.Core.Auth;
using LibraryService.Domain.Core.Extensions;
using LibraryService.Infrastructure.Data.Repositories;
using LibraryService.Services.Interfaces;

#endregion

namespace LibraryService.Infrastructure.Business
{
    public class IssueService : IIssueService
    {
        private readonly ReserveRepo _reserveRepo;
        private readonly IssueRepo _issueRepo;
        private readonly BookInstanceRepo _bookInstanceRepo;

        public IssueService(BookInstanceRepo bookInstanceRepo, IssueRepo issueRepo, ReserveRepo reserveRepo)
        {
            _bookInstanceRepo = bookInstanceRepo;
            _issueRepo = issueRepo;
            _reserveRepo = reserveRepo;
        }

        // Выдача книги без брони
        public async Task MakeIssue(Book book, User user)
        {
            // Проверка на наличие экземпляров книги (сверить количество выданых и забронированных)
            var instanceCount = _bookInstanceRepo.GetListQuery().Count(p => p.IdBook == book.Id);
            var reservesCount = _reserveRepo.GetListQuery().Count(p =>
                p.IdBook == book.Id && p.IsActive && p.Date.AddDays(1) < DateTimeExtension.CurrentTimeLong());
            var issuesCount = _issueRepo.GetListQuery().Count(p => p.IdBook == book.Id && !p.IsReturned);

            if (instanceCount - reservesCount - issuesCount <= 0) throw new Exception("Свободных книг нет");

            // Добавление брони
            var issue = new Issue(user, book);
            await _issueRepo.AddAsync(issue);
        }

        // Выдача забронированной книги
        public async Task MakeIssue(Reserve reserve)
        {
            // Проверку на свободные книги делать не нужно, но необходимо снять бронь с книги
            await _reserveRepo.SetInactive(reserve);

            var issue = new Issue(reserve);
            await _issueRepo.AddAsync(issue);
        }

        public async Task ReturnBook(Issue issued)
        {
            // Проверку на свободные книги делать не нужно, но необходимо снять бронь с книги
            await _issueRepo.SetReturned(issued);
        }
    }
}