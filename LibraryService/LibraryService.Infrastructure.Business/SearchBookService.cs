#region

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryService.Domain.Core;
using LibraryService.Domain.Core.Extensions;
using LibraryService.Infrastructure.Data.Repositories;
using LibraryService.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

#endregion

namespace LibraryService.Infrastructure.Business
{
    public class SearchBookService : ISearchBookService
    {
        private readonly BookRepo _bookRepo;
        private readonly AuthorRepo _authorRepo;
        private readonly BookGenreRepo _bookGenreRepo;
        private readonly PublisherRepo _publisherRepo;

        public SearchBookService(PublisherRepo publisherRepo, BookRepo bookRepo, AuthorRepo authorRepo, BookGenreRepo bookGenreRepo)
        {
            _publisherRepo = publisherRepo;
            _bookRepo = bookRepo;
            _authorRepo = authorRepo;
            _bookGenreRepo = bookGenreRepo;
        }

        public async Task<IEnumerable<Book>> FindBooksAsync(int? idAuthor, int? idPublisher, IEnumerable<int> genres,
            int skip = 0, int take = 10)
        {
            Author author = null;
            Publisher publisher = null;
            IEnumerable<BookGenre> bookGenres = null;

            if (genres != null && genres.Any())
            {
                bookGenres = genres
                    .Join(_bookGenreRepo.GetListQuery(),
                        g => g,
                        bg => bg.IdGenre,
                        (g, bg) => bg).ToList();
            }

            var booksQuery = _bookRepo.GetListQuery()
                .Include(p => p.Publisher)
                .Include(p => p.Author);

            if (idAuthor.HasValue && idAuthor.Value != -1) author = await _authorRepo.GetAsync(idAuthor.Value);
            if (idPublisher.HasValue && idPublisher.Value != -1)
                publisher = await _publisherRepo.GetAsync(idPublisher.Value);

            return SearchFilter(booksQuery, author, publisher, bookGenres).Skip(skip * take).Take(take).ToList();
        }

        public async Task<int> BooksCount(int? idAuthor, int? idPublisher, IEnumerable<int> genres)
        {
            Author author = null;
            Publisher publisher = null;
            IEnumerable<BookGenre> bookGenres = null;

            if (genres != null && genres.Any())
            {
                bookGenres = genres
                    .Join(_bookGenreRepo.GetListQuery(),
                        g => g,
                        bg => bg.IdGenre,
                        (g, bg) => bg).ToList();
            }

            var booksQuery = _bookRepo.GetListQuery();

            if (idAuthor.HasValue && idAuthor.Value != -1) author = await _authorRepo.GetAsync(idAuthor.Value);
            if (idPublisher.HasValue && idPublisher.Value != -1)
                publisher = await _publisherRepo.GetAsync(idPublisher.Value);

            return SearchFilter(booksQuery, author, publisher, bookGenres).Count();
        }

        private IEnumerable<Book> SearchFilter(IQueryable<Book> bookQuery, Author author, Publisher publisher,
            IEnumerable<BookGenre> genres)
        {
            return bookQuery.SearchByAuthor(author).SearchByPublisher(publisher).SearchByGenres(genres);
        }
    }
}