using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryService.Domain.Core;
using LibraryService.Domain.Core.Auth;
using LibraryService.Domain.Interfaces;
using LibraryService.Infrastructure.Data.Repositories;
using LibraryService.Services.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace LibraryService.Infrastructure.Business
{
    public class UserService : IUserService
    {
        private readonly IEmailSender _emailSender;
        private readonly UsersRepo _usersRepo;
        private readonly UserManager<User> _userManager;

        public UserService(IEmailSender emailSender, UsersRepo usersRepo, UserManager<User> userManager)
        {
            _emailSender = emailSender;
            _usersRepo = usersRepo;
            _userManager = userManager;
        }

        public async Task AddUser(User user, IEnumerable<string> roles, string password)
        {
            if (!_usersRepo.IsUnique(user))
            {
                throw new Exception("Пользователь с таким email уже существует");
            }
            
            var result = await _usersRepo.AddAsync(user, password);
            if (!result.Succeeded) throw new Exception(string.Join("\n", result.Errors));
            
            var addedUser = await _usersRepo.GetAsync(user.Id);
            await _usersRepo.AddRoles(addedUser, roles);

            var message = $"login: {user.Email}<br>password:{password}";
            await _emailSender.SendEmailAsync(user.Email, "Учетная запись Libro", message);
            throw new Exception(string.Join("\n", result.Errors));
        }

        public async Task UpdateUser(User user, IEnumerable<string> roles)
        {
            var result = await _usersRepo.UpdateAsync(user);
            if(!result.Succeeded) throw new Exception(string.Join("\n", result.Errors));
            var currentRoles = await _usersRepo.GetUserRoles(user);

            var addRoles = roles.Except(currentRoles).ToList();
            var removeRoles = currentRoles.Except(roles).ToList();

            var addResult = await _usersRepo.AddRoles(user, addRoles);
            var removeResult = await _usersRepo.RemoveRoles(user, removeRoles);

            if (!(addResult.Succeeded && removeResult.Succeeded))
            {
                throw new Exception("При сохранении ролей произошла ошибка");
            }
        }

        public async Task RemoveUser(User user)
        {
            var result = await _usersRepo.DeleteAsync(user);

            if (!result.Succeeded) throw new Exception(string.Join("\n", result.Errors));
            
            var message = $"Ваша учетная запись была удалена.";
            await _emailSender.SendEmailAsync(user.Email, "Учетная запись Libro", message);
        }

        public async Task ChangePassword(int idUser, string newPassword, string code)
        {
            var user = await _usersRepo.GetAsync(idUser);
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            var result = await _userManager.ResetPasswordAsync(user, code, newPassword);
            if (!result.Succeeded) throw new Exception(string.Join("\n", result.Errors));

            var message = $"Пароль был изменен.\nВаш новый пароль: {newPassword}";
            await _emailSender.SendEmailAsync(user.Email, "Учетная запись Libro", message);

        }
    }
}