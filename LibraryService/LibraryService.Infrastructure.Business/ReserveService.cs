#region

using System;
using System.Linq;
using System.Threading.Tasks;
using LibraryService.Domain.Core;
using LibraryService.Domain.Core.Auth;
using LibraryService.Infrastructure.Data.Repositories;
using LibraryService.Services.Interfaces;

#endregion

namespace LibraryService.Infrastructure.Business
{
    public class ReserveService : IReserveService
    {
        private readonly ReserveRepo _reserveRepo;
        private readonly BookInstanceRepo _bookInstanceRepo;

        public ReserveService(ReserveRepo reserveRepo, BookInstanceRepo bookInstanceRepo)
        {
            _reserveRepo = reserveRepo;
            _bookInstanceRepo = bookInstanceRepo;
        }

        public async Task MakeReserve(Book book, User user)
        {
            var bookInStocks = _bookInstanceRepo.GetBooksInStock().FirstOrDefault(p => p.IdBook == book.Id);

            // Проверка на наличие экземпляров книги (проверить в выданых и бронированных)
            if (bookInStocks != null)
            {
                var instanceCount = bookInStocks.InstancesCount;
                var reservesCount = bookInStocks.ReservesCount;
                var issuesCount = bookInStocks.IssuesCount;

                if (instanceCount - reservesCount - issuesCount <= 0) throw new Exception("Свободных книг нет");
            }

            // Добавление брони
            var reserve = new Reserve(user, book);
            await _reserveRepo.AddAsync(reserve);
        }

        public async Task CancelReserve(int idReserve)
        {
            var reserve = await _reserveRepo.GetAsync(idReserve);
            await _reserveRepo.SetInactive(reserve);
        }
    }
}