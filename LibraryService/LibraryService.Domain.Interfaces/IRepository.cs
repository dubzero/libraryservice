﻿#region

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryService.Domain.Core;

#endregion

namespace LibraryService.Domain.Interfaces
{
    public interface IRepository<T> where T : BaseEntity
    {
        IQueryable<T> GetListQuery();
        IEnumerable<T> GetList();
        T Add(T entity);
        IEnumerable<T> AddRange(IEnumerable<T> entities);
        bool Delete(T entity);
        bool DeleteRange(IEnumerable<T> entities);
        T Update(T entity);
        T Get(int id);
        bool IsUnique(T entity);
        Task<IEnumerable<T>> GetListAsync();
        Task<T> AddAsync(T entity);
        Task<IEnumerable<T>> AddRangeAsync(IEnumerable<T> entities);
        Task<T> GetAsync(int id);
    }
}