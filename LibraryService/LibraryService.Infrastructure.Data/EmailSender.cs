using System;
using System.Net;
using MailKit.Net.Smtp;
using System.Threading.Tasks;
using LibraryService.Domain.Interfaces;
using MimeKit;

namespace LibraryService.Infrastructure.Data
{
    public class EmailSender : IEmailSender
    {
        
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();
 
            emailMessage.From.Add(new MailboxAddress("Администрация сайта", "libro@internet.ru"));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };
             
            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.mail.ru", 465, true);
                await client.AuthenticateAsync("libro@internet.ru", "AyY1UptEyr)9");
                await client.SendAsync(emailMessage);
 
                await client.DisconnectAsync(true);
            }
            Console.WriteLine("Письмо отправлено");
        }

    }
}