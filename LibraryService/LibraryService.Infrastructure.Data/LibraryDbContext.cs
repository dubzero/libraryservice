﻿#region

using System;
using LibraryService.Domain.Core;
using LibraryService.Domain.Core.Auth;
using LibraryService.Domain.Core.QueryTypes;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

#endregion

namespace LibraryService.Infrastructure.Data
{
    public class LibraryDbContext : IdentityDbContext<User, Role, int>
    {
        public LibraryDbContext(DbContextOptions<LibraryDbContext> options) : base(options)
        {
        }

        public virtual DbSet<Book> Books { get; set; }
        public virtual DbSet<BookInstance> BookInstances { get; set; }

        public virtual DbSet<Genre> Genres { get; set; }
        public virtual DbSet<Publisher> Publishers { get; set; }
        public virtual DbSet<Author> Authors { get; set; }
        public virtual DbSet<BookGenre> BookGenres { get; set; }

        public virtual DbSet<Reserve> Reserves { get; set; }
        public virtual DbSet<Issue> Issues { get; set; }

        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<BookRate> BookRates { get; set; }

        public virtual DbSet<BookInStock> BooksInStock { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
#if DEBUG
            // Для проверки того что Linq запрос компилируется
            optionsBuilder.LogTo(Console.WriteLine);
#endif
        }

        public bool IsInMemoryDb()
        {
            return Database.IsInMemory();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BookInStock>(entity =>
            {
                if (!Database.IsInMemory())
                {
                    modelBuilder.Entity<BookInStock>().HasNoKey().ToView("BooksInStock_View");
                }
                else
                {
                    entity.HasKey(e => e.IdBook);
                }
            });
            

            modelBuilder.Entity<Book>()
                .HasQueryFilter(post => EF.Property<bool>(post, nameof(BaseEntity.Deleted)) == false);
            modelBuilder.Entity<BookInstance>()
                .HasQueryFilter(post => EF.Property<bool>(post, nameof(BaseEntity.Deleted)) == false);

            modelBuilder.Entity<Reserve>()
                .HasQueryFilter(post => EF.Property<bool>(post, nameof(BaseEntity.Deleted)) == false);
            modelBuilder.Entity<Issue>()
                .HasQueryFilter(post => EF.Property<bool>(post, nameof(BaseEntity.Deleted)) == false);
            modelBuilder.Entity<Comment>()
                .HasQueryFilter(post => EF.Property<bool>(post, nameof(BaseEntity.Deleted)) == false);
            modelBuilder.Entity<BookRate>()
                .HasQueryFilter(post => EF.Property<bool>(post, nameof(BaseEntity.Deleted)) == false);

            modelBuilder.Entity<Genre>()
                .HasQueryFilter(post => EF.Property<bool>(post, nameof(BaseEntity.Deleted)) == false);
            modelBuilder.Entity<Publisher>()
                .HasQueryFilter(post => EF.Property<bool>(post, nameof(BaseEntity.Deleted)) == false);
            modelBuilder.Entity<Author>()
                .HasQueryFilter(post => EF.Property<bool>(post, nameof(BaseEntity.Deleted)) == false);
            modelBuilder.Entity<BookGenre>()
                .HasQueryFilter(post => EF.Property<bool>(post, nameof(BaseEntity.Deleted)) == false);

            RelationsSet(modelBuilder); // Установка релиационных связей


            base.OnModelCreating(modelBuilder);
        }

        private void RelationsSet(ModelBuilder modelBuilder)
        {
            // Читатель - выдача - книга
            modelBuilder
                .Entity<User>()
                .HasMany(p => p.IssuedBooks)
                .WithMany(p => p.Issuers)
                .UsingEntity<Issue>(
                    j =>
                        j.HasOne(p => p.Book)
                            .WithMany(p => p.IssueHistory)
                            .HasForeignKey(p => p.IdBook),
                    j =>
                        j.HasOne(p => p.User)
                            .WithMany(p => p.IssueHistory)
                            .HasForeignKey(p => p.IdUser),
                    j => { j.HasKey(t => t.Id); });

            // Читатель - бронь - книга
            modelBuilder
                .Entity<User>()
                .HasMany(p => p.ReservedBooks)
                .WithMany(p => p.Reservers)
                .UsingEntity<Reserve>(
                    j =>
                        j.HasOne(p => p.Book)
                            .WithMany(p => p.ReserveHistory)
                            .HasForeignKey(p => p.IdBook),
                    j =>
                        j.HasOne(p => p.User)
                            .WithMany(p => p.ReserveHistory)
                            .HasForeignKey(p => p.IdUser),
                    j => { j.HasKey(t => t.Id); });


            // Книги - оценки
            modelBuilder
                .Entity<BookRate>()
                .HasOne(p => p.Book)
                .WithMany(p => p.BookRates)
                .HasForeignKey(p => p.IdBook);

            // Читатели - оценки
            modelBuilder
                .Entity<BookRate>()
                .HasOne(p => p.User)
                .WithMany()
                .HasForeignKey(p => p.IdUser);

            // Книги - Отзывы
            modelBuilder
                .Entity<Comment>()
                .HasOne(p => p.Book)
                .WithMany(p => p.Comments)
                .HasForeignKey(p => p.IdBook);

            // Книги - читатели
            modelBuilder
                .Entity<Comment>()
                .HasOne(p => p.User)
                .WithMany()
                .HasForeignKey(p => p.IdUser);

            // Книга - Жанры книг - Жанр
            modelBuilder
                .Entity<Book>()
                .HasMany(p => p.Genres)
                .WithMany(p => p.Books)
                .UsingEntity<BookGenre>(
                    j =>
                        j.HasOne(p => p.Genre)
                            .WithMany(p => p.BookGenres)
                            .HasForeignKey(p => p.IdGenre),
                    j =>
                        j.HasOne(p => p.Book)
                            .WithMany(p => p.BookGenres)
                            .HasForeignKey(p => p.IdBook),
                    j => { j.HasKey(t => new {t.IdGenre, t.IdBook}); });

            // Издатели - книги
            modelBuilder
                .Entity<Publisher>()
                .HasMany(p => p.Books)
                .WithOne(p => p.Publisher)
                .HasForeignKey(p => p.IdPublisher);

            // Автор - книги
            modelBuilder
                .Entity<Author>()
                .HasMany(p => p.Books)
                .WithOne(p => p.Author)
                .HasForeignKey(p => p.IdAuthor);

            // Кинга - экземпляр книги
            modelBuilder
                .Entity<BookInstance>()
                .HasOne(p => p.Book)
                .WithMany(p => p.BookInstances)
                .HasForeignKey(p => p.IdBook);
        }
    }
}