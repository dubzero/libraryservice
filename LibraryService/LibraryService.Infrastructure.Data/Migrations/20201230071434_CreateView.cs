﻿#region

using LibraryService.Infrastructure.Data.NoKeyEntitySql;
using Microsoft.EntityFrameworkCore.Migrations;

#endregion

namespace LibraryService.Infrastructure.Data.Migrations
{
    public partial class CreateView : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(BookInStockSql.CreateView);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP VIEW IF EXISTS public.\"BooksInStock_View\"");
        }
    }
}