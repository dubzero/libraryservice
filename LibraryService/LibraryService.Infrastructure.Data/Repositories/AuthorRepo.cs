#region

using System.Threading.Tasks;
using LibraryService.Domain.Core;
using Microsoft.EntityFrameworkCore;

#endregion

namespace LibraryService.Infrastructure.Data.Repositories
{
    public class AuthorRepo : AbstractRepo<Author>
    {
        public AuthorRepo(LibraryDbContext db) : base(db)
        {
        }

        public override async Task<bool> IsDuplicate(Author entity)
        {
            return await GetListQuery().AnyAsync(p => p.Id != entity.Id && p.FullName == entity.FullName);
        }
    }
}