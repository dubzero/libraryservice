#region

using System.Threading.Tasks;
using LibraryService.Domain.Core;
using Microsoft.EntityFrameworkCore;

#endregion

namespace LibraryService.Infrastructure.Data.Repositories
{
    public class PublisherRepo : AbstractRepo<Publisher>
    {
        public PublisherRepo(LibraryDbContext db) : base(db)
        {
        }

        public override async Task<bool> IsDuplicate(Publisher entity)
        {
            return await GetListQuery().AnyAsync(p => p.Id != entity.Id && p.Name == entity.Name);
        }
    }
}