#region

using System.Threading.Tasks;
using LibraryService.Domain.Core;
using Microsoft.EntityFrameworkCore;

#endregion

namespace LibraryService.Infrastructure.Data.Repositories
{
    public class BookRepo : AbstractRepo<Book>
    {
        public BookRepo(LibraryDbContext db) : base(db)
        {
        }

        public override async Task<bool> IsDuplicate(Book entity)
        {
            return await GetListQuery().AnyAsync(p => p.Id != entity.Id &&
                                                      p.IdAuthor == entity.IdAuthor &&
                                                      p.IdPublisher == entity.IdPublisher && p.Title == entity.Title);
        }
    }
}