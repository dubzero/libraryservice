#region

using LibraryService.Domain.Core;

#endregion

namespace LibraryService.Infrastructure.Data.Repositories
{
    public class BookRateRepo : AbstractRepo<BookRate>
    {
        public BookRateRepo(LibraryDbContext db) : base(db)
        {
        }
    }
}