#region

using System.Linq;
using System.Threading.Tasks;
using LibraryService.Domain.Core;
using Microsoft.EntityFrameworkCore;

#endregion

namespace LibraryService.Infrastructure.Data.Repositories
{
    public class IssueRepo : AbstractRepo<Issue>
    {
        public IssueRepo(LibraryDbContext db) : base(db)
        {
        }

        public IQueryable<Issue> GetIssuedBooks(int? idUser)
        {
            if (!idUser.HasValue)
                return GetListQuery()
                    .Where(p => !p.IsReturned);

            return GetListQuery()
                .Include(p => p.Book).Where(p => !p.IsReturned && p.IdUser == idUser);
        }

        public async Task SetReturned(Issue issued)
        {
            issued.IsReturned = true;
            await UpdateAsync(issued);
        }
    }
}