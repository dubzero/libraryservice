﻿#region

using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using LibraryService.Domain.Core.Auth;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

#endregion

namespace LibraryService.Infrastructure.Data.Repositories
{
    public class UsersRepo
    {
        private readonly UserManager<User> _userManager;

        public UsersRepo(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        public async Task<IdentityResult> AddAsync(User entity, string password)
        {
            return await _userManager.CreateAsync(entity, password);
        }

        public async Task<IdentityResult> DeleteAsync(User entity)
        {
            var result = await _userManager.DeleteAsync(entity);
            return result;
        }

        public IQueryable<User> GetListQuery()
        {
            return _userManager.Users;
        }

        public async Task<User> GetAsync(int id)
        {
            return await _userManager.Users.SingleOrDefaultAsync(p => p.Id == id);
        }

        public bool IsUnique(User entity)
        {
            return !GetListQuery().Any(p => p.Email == entity.Email && p.Id != entity.Id);
        }

        public async Task<IdentityResult> UpdateAsync(User entity)
        {
            return await _userManager.UpdateAsync(entity);
        }

        public async Task<IdentityResult> AddRoles(User user, IEnumerable<string> roles)
        {
            return await _userManager.AddToRolesAsync(GetListQuery().FirstOrDefault(p => p.Id == user.Id), roles);
        }

        public async Task<IdentityResult> RemoveRoles(User user, IEnumerable<string> roles)
        {
            return await _userManager.RemoveFromRolesAsync(GetListQuery().FirstOrDefault(p => p.Id == user.Id), roles);
        }

        public async Task<IList<string>> GetUserRoles(User user)
        {
            return await _userManager.GetRolesAsync(user);
        }

        public async Task<User> GetCurrentUser(ClaimsPrincipal claims)
        {
            return await _userManager.GetUserAsync(claims);
        }
    }
}