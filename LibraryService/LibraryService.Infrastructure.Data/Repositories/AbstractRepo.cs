﻿#region

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryService.Domain.Core;
using LibraryService.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;

#endregion

namespace LibraryService.Infrastructure.Data.Repositories
{
    public abstract class AbstractRepo<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly LibraryDbContext _db;

        protected AbstractRepo(LibraryDbContext db)
        {
            _db = db;
        }

        public virtual IQueryable<T> GetListQuery()
        {
            return _db.Set<T>().AsQueryable();
        }

        public virtual IEnumerable<T> GetList()
        {
            return GetListQuery().ToList();
        }

        public virtual T Add(T entity)
        {
            _db.Add(entity);
            _db.SaveChanges();
            return entity;
        }

        public virtual bool Delete(T entity)
        {
            bool deleteFlag = false;
            try
            {
                entity.Deleted = true;
                _db.Update(entity);
                _db.SaveChanges();
                deleteFlag = true;
            }
            catch
            {
                return deleteFlag;
            }

            return deleteFlag;
        }

        public virtual T Update(T entity)
        {
            _db.Update(entity);
            _db.SaveChanges();
            return entity;
        }

        public virtual T Get(int id)
        {
            var entity = _db.Find<T>(id);
            return !entity.Deleted ? entity : null;
        }

        public virtual bool IsUnique(T entity)
        {
            return !GetList().Any(p => p.Equal(entity) && p.Id != entity.Id);
        }

        public virtual async Task<bool> IsDuplicate(T entity)
        {
            return IsUnique(entity);
        }

        public IEnumerable<T> AddRange(IEnumerable<T> entities)
        {
            _db.AddRange(entities);
            _db.SaveChanges();
            return entities;
        }

        public bool DeleteRange(IEnumerable<T> entities)
        {
            bool deleteFlag = false;
            try
            {
                foreach (var entity in entities)
                {
                    entity.Deleted = true;
                }

                _db.UpdateRange(entities);
                _db.SaveChanges();
                deleteFlag = true;
            }
            catch
            {
                return deleteFlag;
            }

            return deleteFlag;
        }

        public virtual async Task<IEnumerable<T>> GetListAsync()
        {
            return await GetListQuery().ToListAsync();
        }

        public virtual async Task<T> AddAsync(T entity)
        {
            await _db.AddAsync(entity);
            await _db.SaveChangesAsync();
            return entity;
        }

        public virtual async Task<T> UpdateAsync(T entity)
        {
            _db.Update(entity);
            await _db.SaveChangesAsync();
            return entity;
        }

        public async Task<IEnumerable<T>> AddRangeAsync(IEnumerable<T> entities)
        {
            await _db.AddRangeAsync(entities);
            await _db.SaveChangesAsync();
            return entities;
        }

        public virtual async Task<T> GetAsync(int id)
        {
            var entity = await _db.FindAsync<T>(id);
            return !entity.Deleted ? entity : null;
        }
    }
}