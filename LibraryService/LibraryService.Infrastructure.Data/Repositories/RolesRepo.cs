﻿#region

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryService.Domain.Core.Auth;
using Microsoft.AspNetCore.Identity;

#endregion

namespace LibraryService.Infrastructure.Data.Repositories
{
    public class RolesRepo
    {
        public readonly RoleManager<Role> _roleManager;

        public RolesRepo(RoleManager<Role> roleManager)
        {
            _roleManager = roleManager;
        }

        public async Task<IdentityResult> AddAsync(Role entity)
        {
            return await _roleManager.CreateAsync(entity);
        }

        public async Task<IdentityResult> Delete(Role entity)
        {
            return await _roleManager.DeleteAsync(entity);
        }

        public IEnumerable<Role> GetList()
        {
            return GetListQuery().Where(p => p.Name != "Admin").ToList();
            ;
        }

        public IEnumerable<string> GetRolesDescription(IEnumerable<string> userRoles)
        {
            var roles = GetList();

            return userRoles.Join(roles,
                ur => ur,
                r => r.Name,
                (ur, r) => r.Description);
        }

        public async Task<IdentityResult> UpdateAsync(Role entity)
        {
            return await _roleManager.UpdateAsync(entity);
        }

        public IQueryable<Role> GetListQuery()
        {
            return _roleManager.Roles;
        }

        public bool IsUnique(Role entity)
        {
            if (entity.Name != null && entity.Id != 0)
            {
                return !GetListQuery().Any(p => p.NormalizedName == entity.Name.ToUpper() && p.Id != entity.Id);
            }

            if (entity.Name != null)
            {
                return !GetListQuery().Any(p => p.NormalizedName == entity.Name.ToUpper());
            }

            return true;
        }
    }
}