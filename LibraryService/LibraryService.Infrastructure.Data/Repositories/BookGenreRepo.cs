#region

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryService.Domain.Core;

#endregion

namespace LibraryService.Infrastructure.Data.Repositories
{
    public class BookGenreRepo : AbstractRepo<BookGenre>
    {
        public BookGenreRepo(LibraryDbContext db) : base(db)
        {
        }

        public async Task UpdateBookGeneres(Book book, IEnumerable<int> genres)
        {
            // Получаем имеющиеся
            var currentGenres = GetListQuery().Where(p => p.IdBook == book.Id).ToList();

            // Удалить лишние
            var delGenresIds = currentGenres
                .Where(p => p.IdGenre != null)
                .Select(p => p.IdGenre.Value)
                .Except(genres).ToList();
            if (delGenresIds.Any())
            {
                var delGenres = delGenresIds
                    .Join(GetListQuery().Where(p => p.IdBook == book.Id),
                        g => g,
                        bg => bg.IdGenre,
                        (g, bg) => bg).ToList();
                if (delGenres.Any()) DeleteRange(delGenres);
            }

            // Добавить новые
            var addGenresIds = genres
                .Except(currentGenres.Where(p => p.IdGenre != null)
                    .Select(p => p.IdGenre.Value)).ToList();
            if (addGenresIds.Any())
            {
                var addGenres = addGenresIds.Select(p => new BookGenre {IdBook = book.Id, IdGenre = p});
                if (addGenres.Any()) await AddRangeAsync(addGenres);
            }
        }
    }
}