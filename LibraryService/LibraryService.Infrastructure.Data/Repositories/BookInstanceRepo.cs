#region

using System.Linq;
using LibraryService.Domain.Core;
using LibraryService.Domain.Core.QueryTypes;

#endregion

namespace LibraryService.Infrastructure.Data.Repositories
{
    public class BookInstanceRepo : AbstractRepo<BookInstance>
    {
        public BookInstanceRepo(LibraryDbContext db) : base(db)
        {
        }

        public IQueryable<BookInStock> GetBooksInStock()
        {
            return _db.BooksInStock.AsQueryable();
        }
    }
}