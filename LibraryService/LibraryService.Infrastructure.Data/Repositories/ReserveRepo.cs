#region

using System.Linq;
using System.Threading.Tasks;
using LibraryService.Domain.Core;
using Microsoft.EntityFrameworkCore;

#endregion

namespace LibraryService.Infrastructure.Data.Repositories
{
    public class ReserveRepo : AbstractRepo<Reserve>
    {
        public ReserveRepo(LibraryDbContext db) : base(db)
        {
        }

        public IQueryable<Reserve> GetReservedBooks(int? idUser)
        {
            if (!idUser.HasValue)
                return GetListQuery()
                    .Where(p => p.IsActive)
                    .Include(p => p.Book);

            return GetListQuery()
                .Include(p => p.Book).Where(p => p.IsActive && p.IdUser == idUser);
        }

        public async Task SetInactive(Reserve reserve)
        {
            reserve.IsActive = false;
            await UpdateAsync(reserve);
        }
    }
}