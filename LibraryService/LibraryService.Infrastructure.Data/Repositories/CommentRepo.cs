#region

using LibraryService.Domain.Core;

#endregion

namespace LibraryService.Infrastructure.Data.Repositories
{
    public class CommentRepo : AbstractRepo<Comment>
    {
        public CommentRepo(LibraryDbContext db) : base(db)
        {
        }
    }
}