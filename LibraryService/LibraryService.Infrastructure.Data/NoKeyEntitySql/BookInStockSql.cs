namespace LibraryService.Infrastructure.Data.NoKeyEntitySql
{
    public static class BookInStockSql
    {
        public const string CreateView = "create view \"BooksInStock_View\"(\"IdBook\", \"InstancesCount\", \"ReservesCount\", \"IssuesCount\") as\r" +
                                          "\nSELECT instances.\"IdBook\",\r" +
                                          "\n       count(instances.\"IdBook\")                                  AS \"InstancesCount\",\r" +
                                          "\n       count(reserves.\"IdBook\") FILTER (WHERE reserves.\"Date\" > (date_part('epoch'::text, now())::integer - 86400))    AS \"ReservesCount\",\r" +
                                          "\n       count(issues.\"IdBook\")                                     AS \"IssuesCount\"\r\nFROM \"BookInstances\" instances\r" +
                                          "\n         LEFT JOIN \"Reserves\" reserves ON instances.\"IdBook\" = reserves.\"IdBook\" AND reserves.\"IsActive\" IS TRUE AND\r" +
                                          "\n                                          reserves.\"Deleted\" IS FALSE\r" +
                                          "\n         LEFT JOIN \"Issues\" issues ON issues.\"IdBook\" = instances.\"IdBook\" AND issues.\"IsReturned\" IS FALSE AND\r" +
                                          "\n                                      issues.\"Deleted\" IS FALSE\r" +
                                          "\n" +
                                          "GROUP BY instances.\"IdBook\";";

    }
}