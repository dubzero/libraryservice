#region

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#endregion

namespace LibraryService.Domain.Core
{
    public class Genre : BaseEntity
    {
        public Genre()
        {
        }

        public Genre(string name)
        {
            Name = name;
        }

        [Required] public string Name { get; set; }

        public ICollection<BookGenre> BookGenres { get; set; }
        public ICollection<Book> Books { get; set; }
    }
}