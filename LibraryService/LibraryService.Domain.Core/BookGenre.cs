namespace LibraryService.Domain.Core
{
    public class BookGenre : BaseEntity
    {
        public int? IdBook { get; set; }
        public virtual Book Book { get; set; }

        public int? IdGenre { get; set; }
        public virtual Genre Genre { get; set; }
    }
}