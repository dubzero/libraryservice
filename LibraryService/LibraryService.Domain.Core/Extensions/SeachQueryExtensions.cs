using System.Collections.Generic;
using System.Linq;

namespace LibraryService.Domain.Core.Extensions
{
    public static class SeachQueryExtensions
    {
        public static IQueryable<Book> SearchByAuthor(this IQueryable<Book> books, Author author)
        {
            if (author != null && author.Id != 0)
            {
                return books.Where(p => p.IdAuthor == author.Id);
            }

            return books;
        }

        public static IQueryable<Book> SearchByPublisher(this IQueryable<Book> books, Publisher publisher)
        {
            if (publisher != null && publisher.Id != 0)
            {
                return books.Where(p => p.IdPublisher == publisher.Id);
            }

            return books;
        }

        public static IEnumerable<Book> SearchByGenres(this IQueryable<Book> books, IEnumerable<BookGenre> genres)
        {
            if (genres != null)
            {
                return genres.Join(books,
                    g => g.IdBook,
                    b => b.Id,
                    (g, b) => b);
            }

            return books;
        }
    }
}