﻿#region

using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

#endregion

namespace LibraryService.Domain.Core.Extensions
{
    public static class ViewModelExtensions
    {
        public static string GetPropertyTitle(this object model, string Name)
        {
            var type = model.GetType();

            if (Attribute.IsDefined(type.GetProperty(Name), typeof(DisplayAttribute)))
                return (Attribute.GetCustomAttribute(type.GetProperty(Name), typeof(DisplayAttribute)) as
                    DisplayAttribute).Name;
            if (Attribute.IsDefined(type.GetProperty(Name), typeof(DescriptionAttribute)))
                return (Attribute.GetCustomAttribute(type.GetProperty(Name), typeof(DescriptionAttribute)) as
                    DescriptionAttribute).Description;
            return Name;
        }

        public static string GetPropertyName(this object model, string Name)
        {
            var type = model.GetType();
            if (Attribute.IsDefined(type.GetProperty(Name), typeof(JsonPropertyNameAttribute)))
                return (Attribute.GetCustomAttribute(type.GetProperty(Name), typeof(JsonPropertyNameAttribute)) as
                    JsonPropertyNameAttribute).Name;
            return Name;
        }
    }
}