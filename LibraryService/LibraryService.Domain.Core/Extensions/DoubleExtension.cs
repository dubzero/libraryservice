#region

using System;
using System.Globalization;

#endregion

namespace LibraryService.Domain.Core.Extensions
{
    public static class DoubleExtension
    {
        public static double? Str2Double(this string str)
        {
            str = str.Replace(".", ",").Replace("га", "").Replace(" ", "").Trim();
            var provider = new CultureInfo("ru-RU");

            if (double.TryParse(str, NumberStyles.AllowDecimalPoint, provider, out double result))
            {
                return result;
            }

            throw new ArgumentException("Неправильная строка");
        }

        public static string Double2Str(this double? value)
        {
            return value.HasValue ? value.Value.ToString("F2", CultureInfo.CurrentCulture) : "";
        }

        public static string Decimal2Str(this double value, string format)
        {
            return value.ToString(format);
        }
    }
}