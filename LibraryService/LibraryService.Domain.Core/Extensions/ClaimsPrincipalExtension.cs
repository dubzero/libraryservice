#region

using System.Security.Claims;

#endregion

namespace LibraryService.Domain.Core.Extensions
{
    public static class ClaimsPrincipalExtension
    {
        public const string AdminLevelString = "Admin";
        public const string LibrarianLevelString = "Admin,Librarian";
        public const string ReaderLevelString = "Admin,Librarian,Reader";

        public static bool IsAdmin(this ClaimsPrincipal claims)
        {
            return claims.IsInRole("Admin");
        }

        public static bool IsLibrarian(this ClaimsPrincipal claims)
        {
            return claims.IsInRole("Librarian");
        }

        public static bool IsReader(this ClaimsPrincipal claims)
        {
            return claims.IsInRole("Reader");
        }

        public static bool AdminLevelAccess(this ClaimsPrincipal claims)
        {
            return claims.IsAdmin();
        }


        public static bool LibrarianLevelAccess(this ClaimsPrincipal claims)
        {
            return claims.IsAdmin() || claims.IsLibrarian();
        }


        public static bool ReaderLevelAccess(this ClaimsPrincipal claims)
        {
            return claims.IsAdmin() || claims.IsLibrarian() || claims.IsReader();
        }
    }
}