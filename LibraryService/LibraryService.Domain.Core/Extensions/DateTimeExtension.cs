﻿#region

using System;
using System.Globalization;

#endregion

namespace LibraryService.Domain.Core.Extensions
{
    public static class DateTimeExtension
    {
        public const long UnixDay = 86400;

        public static long CurrentTimeLong()
        {
            return DateTime.UtcNow.DateTimeToLong();
        }

        public static long AddDays(this long date, int days)
        {
            return date + UnixDay * days;
        }

        public static string CurrentTimeString(string format = "dd.MM.yyyy")
        {
            return DateTime.UtcNow.DateTimeToString(format);
        }

        public static string UnixTimeToString(this long date)
        {
            string format = "dd.MM.yyyy";
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(date).ToLocalTime();
            return dtDateTime.DateTimeToString(format);
        }

        public static string UnixTimeToString(this long? date)
        {
            if (date.HasValue)
            {
                string format = "dd.MM.yyyy";
                DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                dtDateTime = dtDateTime.AddSeconds(date.Value).ToLocalTime();
                return dtDateTime.DateTimeToString(format);
            }

            return "";
        }

        public static string UnixTimeToISO(this long? date)
        {
            if (date.HasValue)
            {
                string format = "yyyy-MM-dd";
                DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                dtDateTime = dtDateTime.AddSeconds(date.Value).ToLocalTime();
                return dtDateTime.DateTimeToString(format);
            }

            return "";
        }

        public static string UnixTimeToISO(this long date)
        {
            string format = "yyyy-MM-dd";
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(date).ToLocalTime();
            return dtDateTime.DateTimeToString(format);
        }

        public static string UnixTimeToString(this long date, string format)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(date).ToLocalTime();
            return dtDateTime.DateTimeToString(format);
        }

        public static long? StringToUnixTime(this string date)
        {
            if (string.IsNullOrEmpty(date)) return null;

            var dateTime = !date.Contains("/")
                ? DateTime.Parse(date)
                : DateTime.Parse(date, CultureInfo.InvariantCulture);
            return dateTime.ToUniversalTime().DateTimeToLong();
        }

        public static long StringToUnixTime(this string date, string format)
        {
            DateTime dateTime = DateTime.ParseExact(date, format, CultureInfo.InvariantCulture);
            return dateTime.DateTimeToLong();
        }

        public static long DateTimeToLong(this DateTime date)
        {
            return (long) (date.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }

        public static string DateTimeToString(this DateTime date, string format = "dd.MM.yyyy")
        {
            return format == null ? date.ToString(CultureInfo.InvariantCulture) : date.ToString(format);
        }
    }
}