﻿#region

using System;
using System.Globalization;

#endregion

namespace LibraryService.Domain.Core.Extensions
{
    public static class DecimalExtension
    {
        public static decimal? Str2Decimal(this string str)
        {
            str = str.Replace(".", ",").Replace("га", "").Replace(" ", "").Trim();
            var provider = new CultureInfo("ru-RU");

            if (decimal.TryParse(str, NumberStyles.AllowDecimalPoint, provider, out decimal result))
            {
                return result;
            }

            throw new ArgumentException("Неправильная строка");
        }

        public static string Decimal2Str(this decimal? value)
        {
            return value.HasValue ? value.Value.ToString("F2", CultureInfo.CurrentCulture) : "";
        }

        public static string Decimal2Str(this decimal value)
        {
            return value.ToString("F2", CultureInfo.CurrentCulture);
        }

        public static string Decimal2Str(this decimal value, string format)
        {
            return value.ToString(format);
        }
    }
}