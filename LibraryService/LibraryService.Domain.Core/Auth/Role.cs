﻿#region

using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Identity;

#endregion

namespace LibraryService.Domain.Core.Auth
{
    public class Role : IdentityRole<int>
    {
        [Display(Name = "Код")]
        [JsonPropertyName("id")]
        public override int Id { get; set; }

        [Display(Name = "Роль")]
        [Required]
        [JsonPropertyName("name")]
        public override string Name { get; set; }

        [Display(Name = "Описание")]
        [JsonPropertyName("description")]
        public string Description { get; set; }
    }
}