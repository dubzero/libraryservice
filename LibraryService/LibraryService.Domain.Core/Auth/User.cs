﻿#region

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

#endregion

namespace LibraryService.Domain.Core.Auth
{
    public class User : IdentityUser<int>
    {
        [Required] [StringLength(50)] public string FirstName { get; set; }

        [Required] [StringLength(50)] public string LastName { get; set; }

        [StringLength(50)] public string Patronymic { get; set; }

        public bool Deleted { get; set; }

        public ICollection<Book> IssuedBooks { get; set; }

        public ICollection<Book> ReservedBooks { get; set; }

        public ICollection<Reserve> ReserveHistory { get; set; }

        public ICollection<Issue> IssueHistory { get; set; }

        // Александр Сергеевич
        public string GetAppealName => $"{FirstName} {Patronymic}";

        // Иванов Александр Сергеевич
        public string GetFullName => $"{LastName} {FirstName} {Patronymic}";
    }
}