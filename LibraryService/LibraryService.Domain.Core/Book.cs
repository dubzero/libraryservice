﻿#region

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using LibraryService.Domain.Core.Auth;

#endregion

namespace LibraryService.Domain.Core
{
    public class Book : BaseEntity
    {
        public Book()
        {
            
        }

        public Book(string title)
        {
            Title = title;
        }
        
        [Required] public string Title { get; set; }

        public string SubTitle { get; set; }

        public long? Published { get; set; }

        public int? Pages { get; set; }

        public string Description { get; set; }

        public string ISBN { get; set; }

        public string ImageLink { get; set; } = "../assets/default-book.png";

        public int? IdPublisher { get; set; }
        public virtual Publisher Publisher { get; set; }

        public int? IdAuthor { get; set; }
        public virtual Author Author { get; set; }

        public ICollection<User> Issuers { get; set; }
        public ICollection<User> Reservers { get; set; }
        public ICollection<Issue> IssueHistory { get; set; }
        public ICollection<Reserve> ReserveHistory { get; set; }
        public virtual ICollection<BookRate> BookRates { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }

        public virtual ICollection<BookGenre> BookGenres { get; set; }
        public virtual ICollection<Genre> Genres { get; set; }

        public virtual ICollection<BookInstance> BookInstances { get; set; }
    }
}