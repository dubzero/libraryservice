#region

using System.ComponentModel.DataAnnotations;
using LibraryService.Domain.Core.Auth;

#endregion

namespace LibraryService.Domain.Core
{
    public class Comment : BaseEntity
    {
        [Required] public string Text { get; set; }

        public int? IdBook { get; set; }
        public virtual Book Book { get; set; }

        public int? IdUser { get; set; }
        public virtual User User { get; set; }
    }
}