#region

using System.ComponentModel.DataAnnotations;
using LibraryService.Domain.Core.Auth;
using LibraryService.Domain.Core.Extensions;

#endregion

namespace LibraryService.Domain.Core
{
    /// <summary>
    /// Бронь книги
    /// </summary>
    public class Reserve : BaseEntity
    {
        public Reserve()
        {
        }

        public Reserve(User user, Book book)
        {
            Date = DateTimeExtension.CurrentTimeLong();
            IdBook = book.Id;
            IdUser = user.Id;
            IsActive = true;
        }

        [Required] public long Date { get; set; }

        public int? IdBook { get; set; }
        public virtual Book Book { get; set; }

        public int? IdUser { get; set; }
        public virtual User User { get; set; }

        public bool IsActive { get; set; } = true;
    }
}