namespace LibraryService.Domain.Core
{
    /// <summary>
    /// Экземпляр книги (в библиотеке может быть несколько одинаковых книг)
    /// </summary>
    public class BookInstance : BaseEntity
    {
        public BookInstance()
        {
        }

        public BookInstance(Book book)
        {
            IdBook = book.Id;
        }

        public int? IdBook { get; set; }
        public virtual Book Book { get; set; }
    }
}