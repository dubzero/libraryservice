using System.Text.Json.Serialization;

namespace LibraryService.Domain.Core.DataImport
{
    public class BooksImport
    {
        [JsonPropertyName("title")] public string Title { get; set; }

        [JsonPropertyName("subtitle")] public string SubTitle { get; set; }

        [JsonPropertyName("author")] public string Author { get; set; }

        [JsonPropertyName("publisher")] public string Publisher { get; set; }

        [JsonPropertyName("published")] public string Published { get; set; }

        [JsonPropertyName("pages")] public int Pages { get; set; }

        [JsonPropertyName("description")] public string Description { get; set; }

        [JsonPropertyName("isbn")] public string ISBN { get; set; }

        [JsonPropertyName("imageLink")] public string ImageLink { get; set; } = "../assets/default-book.png";
    }
}