﻿#region

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using LibraryService.Domain.Core.Extensions;

#endregion

namespace LibraryService.Domain.Core
{
    public class ErrorInfo
    {
        [JsonPropertyName("message")] public string Message { get; set; }

        [JsonPropertyName("field")] public string Field { get; set; }

        [JsonPropertyName("description")] public string Description { get; set; }

        public ErrorInfo()
        {
        }

        public ErrorInfo(string message, string field, string descr)
        {
            Message = message;
            Field = field;
            Description = descr;
        }

        public ErrorInfo(string message)
        {
            Message = message;
        }
    }

    public static class ErrorList
    {
        public static List<ErrorInfo> GetErrors(object entity)
        {
            List<ErrorInfo> errors = new List<ErrorInfo>();
            try
            {
                var validationContext = new ValidationContext(entity);
                Validator.ValidateObject(entity, validationContext, true);
            }
            catch (ValidationException validationException)
            {
                foreach (var memberName in validationException.ValidationResult.MemberNames)
                {
                    errors.Add(new ErrorInfo
                    {
                        Message = validationException.ValidationResult.ErrorMessage,
                        Field = entity.GetPropertyName(memberName),
                        Description = entity.GetPropertyTitle(memberName)
                    });
                }
            }

            return errors;
        }
    }
}