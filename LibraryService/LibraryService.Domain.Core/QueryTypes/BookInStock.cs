namespace LibraryService.Domain.Core.QueryTypes
{
    public class BookInStock
    {
        public int IdBook { get; set; }

        public int InstancesCount { get; set; }

        public int ReservesCount { get; set; }

        public int IssuesCount { get; set; }
    }
}