#region

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#endregion

namespace LibraryService.Domain.Core
{
    public class Publisher : BaseEntity
    {
        [Required] public string Name { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}