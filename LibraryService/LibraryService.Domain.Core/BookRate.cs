#region

using System.ComponentModel.DataAnnotations;
using LibraryService.Domain.Core.Auth;

#endregion

namespace LibraryService.Domain.Core
{
    public class BookRate : BaseEntity
    {
        [Range(1, 5, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int Star { get; set; }

        public int? IdBook { get; set; }
        public virtual Book Book { get; set; }

        public int? IdUser { get; set; }
        public virtual User User { get; set; }
    }
}