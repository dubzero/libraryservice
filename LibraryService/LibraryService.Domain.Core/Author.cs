#region

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#endregion

namespace LibraryService.Domain.Core
{
    public class Author : BaseEntity
    {
        [Required] public string FullName { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}