#region

using System.Text.Json.Serialization;
using LibraryService.Domain.Core.Auth;
using LibraryService.Domain.Core.Extensions;

#endregion

namespace LibraryService.Domain.Core
{
    /// <summary>
    /// Выдача книги
    /// </summary>
    public class Issue : BaseEntity
    {
        public Issue()
        {
        }

        public Issue(User user, Book book)
        {
            Date = DateTimeExtension.CurrentTimeLong();
            IdBook = book.Id;
            IdUser = user.Id;
        }

        public Issue(Reserve reserve)
        {
            Date = DateTimeExtension.CurrentTimeLong();
            IdBook = reserve.IdBook;
            IdUser = reserve.IdUser;
        }

        [JsonPropertyName("date")] public long Date { get; set; }

        [JsonPropertyName("idBook")] public int? IdBook { get; set; }
        public virtual Book Book { get; set; }

        public bool IsReturned { get; set; }

        [JsonPropertyName("idUser")] public int? IdUser { get; set; }
        public virtual User User { get; set; }
    }
}