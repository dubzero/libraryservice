﻿#region

using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

#endregion

namespace LibraryService.Domain.Core
{
    public abstract class BaseEntity
    {
        [Key]
        [Display(Name = "Код")]
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonIgnore] public virtual bool Deleted { get; set; }

        public virtual bool Equal(object obj)
        {
            if (obj is BaseEntity entity)
            {
                if (Id == entity.Id)
                {
                    return true;
                }

                return false;
            }

            return false;
        }
    }
}