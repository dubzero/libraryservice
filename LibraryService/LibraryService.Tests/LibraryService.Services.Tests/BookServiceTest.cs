using System.Linq;
using System.Threading.Tasks;
using LibraryService.Domain.Core;
using LibraryService.Infrastructure.Business;
using LibraryService.Infrastructure.Data.Repositories;
using LibraryService.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace LibraryService.Tests.LibraryService.Services.Tests
{
    public class BookServiceTest : ServiceBaseTest
    {
        private readonly IBookService _bookService;
        private readonly BookRepo _bookRepo;
        private readonly BookInstanceRepo _bookInstanceRepo;

        public BookServiceTest() : base()
        {
            _bookService = (IBookService) Env.Provider
                .GetService(typeof(IBookService));
            _bookRepo = (BookRepo) Env.Provider
                .GetService(typeof(BookRepo));
            _bookInstanceRepo = (BookInstanceRepo) Env.Provider
                .GetService(typeof(BookInstanceRepo));
        }

        [Fact]
        public async Task MakeTest()
        {
            string title = "Test book";
            var book = new Book(title);

            await _bookService.CreateBook(book);

            book = _bookRepo.GetListQuery().FirstOrDefault(p => p.Title == title);
            Assert.NotNull(book);

            var instance = _bookInstanceRepo.GetListQuery().Where(p => p.IdBook == book.Id).ToList();

            Assert.NotNull(instance);
            Assert.Single(instance);
        }

        [Fact]
        public async Task AddBookInstanceTest()
        {
            var book = _bookRepo.GetListQuery().FirstOrDefault();
            Assert.NotNull(book);

            var instanceCount = _bookInstanceRepo.GetListQuery().Count(p => p.IdBook == book.Id);
            await _bookService.AddBookInstance(book);

            var newCount = _bookInstanceRepo.GetListQuery().Count(p => p.IdBook == book.Id);
            Assert.Equal(instanceCount + 1, newCount);
        }

        [Fact]
        public async Task DeleteBookTest()
        {
            string title = "Test book";
            var book = new Book(title);

            await _bookService.CreateBook(book);

            book = _bookRepo.GetListQuery().FirstOrDefault(p => p.Title == title);
            Assert.NotNull(book);
            int idBook = book.Id;
            var instance = _bookInstanceRepo.GetListQuery().Where(p => p.IdBook == idBook).ToList();

            Assert.NotNull(instance);
            Assert.Single(instance);

            await _bookService.DeleteBook(book);
            
            book = _bookRepo.GetListQuery().FirstOrDefault(p => p.Title == title);
            Assert.Null(book);
            
            instance = _bookInstanceRepo.GetListQuery().Where(p => p.IdBook == idBook).ToList();
            Assert.Empty(instance);
        }
    }
}