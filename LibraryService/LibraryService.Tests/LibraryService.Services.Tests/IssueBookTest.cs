using System.Linq;
using System.Threading.Tasks;
using LibraryService.Infrastructure.Data.Repositories;
using LibraryService.Services.Interfaces;
using Xunit;

namespace LibraryService.Tests.LibraryService.Services.Tests
{
    public class IssueBookTest : ServiceBaseTest
    {
        private readonly IIssueService _issueService;
        private readonly IssueRepo _issueRepo;
        private readonly BookInstanceRepo _bookInstanceRepo;
        private readonly BookRepo _bookRepo;
        private readonly UsersRepo _usersRepo;
        public IssueBookTest() : base()
        {
            _issueService = (IIssueService) Env.Provider
                .GetService(typeof(IIssueService));
            _issueRepo = (IssueRepo) Env.Provider
                .GetService(typeof(IssueRepo));
            _bookInstanceRepo = (BookInstanceRepo) Env.Provider
                .GetService(typeof(BookInstanceRepo));
            _bookRepo = (BookRepo) Env.Provider
                .GetService(typeof(BookRepo));
            _usersRepo = (UsersRepo) Env.Provider
                .GetService(typeof(UsersRepo));
        }

        [Fact]
        public async Task MakeIssueTest()
        {
            var book = _bookRepo.GetListQuery().FirstOrDefault();
            Assert.NotNull(book);
            var user = _usersRepo.GetListQuery().FirstOrDefault();
            Assert.NotNull(user);
            var bookInStock = _bookInstanceRepo.GetBooksInStock().FirstOrDefault(p => p.IdBook == book.Id);
            Assert.NotNull(bookInStock);
            await _issueService.MakeIssue(book, user);
            await Env.UpdateDbViews();
            var bookInStockNew = _bookInstanceRepo.GetBooksInStock().FirstOrDefault(p => p.IdBook == book.Id);
            Assert.NotNull(bookInStockNew);
            
            Assert.Equal(bookInStock.IssuesCount + 1, bookInStockNew.IssuesCount);
            Assert.Equal(bookInStock.InstancesCount, bookInStockNew.InstancesCount);
            Assert.Equal(bookInStock.ReservesCount, bookInStockNew.ReservesCount);
        }
        
        [Fact]
        public async Task ReturnBookTest()
        {
            var book = _bookRepo.GetListQuery().FirstOrDefault();
            var user = _usersRepo.GetListQuery().FirstOrDefault();
            var bookInStock = _bookInstanceRepo.GetBooksInStock().FirstOrDefault(p => p.IdBook == book.Id);
            
            await _issueService.MakeIssue(book, user);
            await Env.UpdateDbViews();
            var bookInStockNew = _bookInstanceRepo.GetBooksInStock().FirstOrDefault(p => p.IdBook == book.Id);
            Assert.NotNull(bookInStockNew);
            
            Assert.Equal(bookInStock.IssuesCount + 1, bookInStockNew.IssuesCount);
            Assert.Equal(bookInStock.InstancesCount, bookInStockNew.InstancesCount);
            Assert.Equal(bookInStock.ReservesCount, bookInStockNew.ReservesCount);

            var issue = _issueRepo.GetListQuery().FirstOrDefault(p => !p.IsReturned && p.IdBook == book.Id);
            await _issueService.ReturnBook(issue);
            await Env.UpdateDbViews();
            bookInStockNew = _bookInstanceRepo.GetBooksInStock().FirstOrDefault(p => p.IdBook == book.Id);
            
            Assert.Equal(bookInStock.IssuesCount, bookInStockNew.IssuesCount);
            Assert.Equal(bookInStock.InstancesCount, bookInStockNew.InstancesCount);
            Assert.Equal(bookInStock.ReservesCount, bookInStockNew.ReservesCount);
        }
    }
}