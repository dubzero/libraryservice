using System.Linq;
using System.Threading.Tasks;
using LibraryService.Domain.Core;
using LibraryService.Domain.Core.Auth;
using LibraryService.Infrastructure.Data.Repositories;
using LibraryService.Services.Interfaces;
using Xunit;

namespace LibraryService.Tests.LibraryService.Services.Tests
{
    public class ReserveBookTest : ServiceBaseTest
    {
        private readonly IReserveService _reserveService;
        private readonly ReserveRepo _reserveRepo;
        private readonly BookInstanceRepo _bookInstanceRepo;
        private readonly BookRepo _bookRepo;
        private readonly UsersRepo _usersRepo;
        public ReserveBookTest() : base()
        {
            _reserveService = (IReserveService) Env.Provider
                .GetService(typeof(IReserveService));
            _reserveRepo = (ReserveRepo) Env.Provider
                .GetService(typeof(ReserveRepo));
            _bookInstanceRepo = (BookInstanceRepo) Env.Provider
                .GetService(typeof(BookInstanceRepo));
            _bookRepo = (BookRepo) Env.Provider
                .GetService(typeof(BookRepo));
            _usersRepo = (UsersRepo) Env.Provider
                .GetService(typeof(UsersRepo));
        }
        
        [Fact]
        public async Task MakeReserveTest()
        {
            var book = _bookRepo.GetListQuery().FirstOrDefault();
            Assert.NotNull(book);
            var user = _usersRepo.GetListQuery().FirstOrDefault();
            Assert.NotNull(user);
            var bookInStock = _bookInstanceRepo.GetBooksInStock().FirstOrDefault(p => p.IdBook == book.Id);
            Assert.NotNull(bookInStock);
            await _reserveService.MakeReserve(book, user);
            await Env.UpdateDbViews();
            var bookInStockNew = _bookInstanceRepo.GetBooksInStock().FirstOrDefault(p => p.IdBook == book.Id);
            Assert.NotNull(bookInStockNew);
            
            Assert.Equal(bookInStock.IssuesCount, bookInStockNew.IssuesCount);
            Assert.Equal(bookInStock.InstancesCount, bookInStockNew.InstancesCount);
            Assert.Equal(bookInStock.ReservesCount + 1, bookInStockNew.ReservesCount);
        }

                
        [Fact]
        public async Task CancelReserveTest()
        {
            var book = _bookRepo.GetListQuery().FirstOrDefault();
            var user = _usersRepo.GetListQuery().FirstOrDefault();
            var bookInStock = _bookInstanceRepo.GetBooksInStock().FirstOrDefault(p => p.IdBook == book.Id);
            Assert.NotNull(bookInStock);
            await _reserveService.MakeReserve(book, user);
            await Env.UpdateDbViews();
            var reserve = _reserveRepo.GetListQuery().FirstOrDefault(p => p.IsActive && p.IdBook == book.Id);
            Assert.NotNull(reserve);

            await _reserveService.CancelReserve(reserve.Id);
            await Env.UpdateDbViews();
            var bookInStockNew = _bookInstanceRepo.GetBooksInStock().FirstOrDefault(p => p.IdBook == book.Id);
            Assert.NotNull(bookInStockNew);
            
            Assert.Equal(bookInStock.IssuesCount, bookInStockNew.IssuesCount);
            Assert.Equal(bookInStock.InstancesCount, bookInStockNew.InstancesCount);
            Assert.Equal(bookInStock.ReservesCount, bookInStockNew.ReservesCount);

            await _reserveService.MakeReserve(book, user);
            await Env.UpdateDbViews();
            await _reserveService.CancelReserve(reserve.Id);
            await Env.UpdateDbViews();
            
            Assert.Equal(bookInStock.IssuesCount, bookInStockNew.IssuesCount);
            Assert.Equal(bookInStock.InstancesCount, bookInStockNew.InstancesCount);
            Assert.Equal(bookInStock.ReservesCount, bookInStockNew.ReservesCount);
            
            reserve = await _reserveRepo.GetAsync(reserve.Id);
            Assert.False(reserve.IsActive);
        }
        
    }
}