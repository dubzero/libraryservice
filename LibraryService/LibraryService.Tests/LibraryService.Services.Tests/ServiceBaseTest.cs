using System.Threading.Tasks;
using Xunit;

namespace LibraryService.Tests.LibraryService.Services.Tests
{
    public abstract class ServiceBaseTest
    {
        protected readonly TestEnvironment Env;
        
        public ServiceBaseTest()
        {
            Env = new TestEnvironment();
        }
    }
}