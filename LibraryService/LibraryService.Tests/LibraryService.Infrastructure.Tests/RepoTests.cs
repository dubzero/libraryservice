using System.Threading.Tasks;
using LibraryService.Domain.Core;
using LibraryService.Infrastructure.Data.Repositories;
using LibraryService.Services.Interfaces;
using Xunit;

namespace LibraryService.Tests.LibraryService.Infrastructure.Tests
{
    public class RepoTests
    {
        private readonly TestEnvironment _env;
        public RepoTests()
        {
            _env = new TestEnvironment();
        }
    
        [Fact]
        public async Task GetBooksListTest()
        {
            var booksRepo = (BookRepo) _env.Provider.GetService(typeof(BookRepo));
            var books = await booksRepo.GetListAsync();
            Assert.NotEmpty(books);
        }
        
        [Fact]
        public async Task AddAuthorTest()
        {
            var authorRepo = (AuthorRepo) _env.Provider.GetService(typeof(AuthorRepo));
            var added = await authorRepo.AddAsync(new Author() {FullName = "Иванов Иван Ивнович"});
            var author = await authorRepo.GetAsync(added.Id);
            Assert.NotNull(author);
            Assert.Equal("Иванов Иван Ивнович", author.FullName);
        }
        
        [Fact]
        public async Task DeleteAuthorTest()
        {
            var authorRepo = (AuthorRepo) _env.Provider.GetService(typeof(AuthorRepo));
            var added = await authorRepo.AddAsync(new Author() {FullName = "Иванов Иван Ивнович"});
            var author = await authorRepo.GetAsync(added.Id);
            Assert.NotNull(author);
            authorRepo.Delete(author);
            author = await authorRepo.GetAsync(added.Id);
            Assert.Null(author);
        }
        
        [Fact]
        public async Task UpdateAuthorTest()
        {
            var authorRepo = (AuthorRepo) _env.Provider.GetService(typeof(AuthorRepo));
            var added = await authorRepo.AddAsync(new Author() {FullName = "Иванов Иван Ивнович"});
            var author = await authorRepo.GetAsync(added.Id);
            Assert.NotNull(author);
            author.FullName = "Update Test";
            await authorRepo.UpdateAsync(author);
            author = await authorRepo.GetAsync(added.Id);
            Assert.Equal("Update Test", author.FullName);
        }
    }
}