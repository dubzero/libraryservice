#region

using LibraryService.Infrastructure.Business;
using LibraryService.Infrastructure.Data.Repositories;
using LibraryService.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

#endregion

namespace LibraryService.Tests
{
    public class InjectionTest
    {
        private readonly TestEnvironment _env;
        public InjectionTest()
        {
            _env = new TestEnvironment();
        }
        
        [Fact]
        public void RepoInjectTest()
        {
            var usersRepo = _env.Provider.GetService(typeof(UsersRepo));
            Assert.NotNull(usersRepo);
            var rolesRepo = _env.Provider.GetService(typeof(RolesRepo));
            Assert.NotNull(rolesRepo);
            var issueRepo = _env.Provider.GetService(typeof(IssueRepo));
            Assert.NotNull(issueRepo);
            var bookRateRepo = _env.Provider.GetService(typeof(BookRateRepo));
            Assert.NotNull(bookRateRepo);
            var reserveRepo = _env.Provider.GetService(typeof(ReserveRepo));
            Assert.NotNull(reserveRepo);
            var bookRepo = _env.Provider.GetService(typeof(BookRepo));
            Assert.NotNull(bookRepo);
            var commentRepo = _env.Provider.GetService(typeof(CommentRepo));
            Assert.NotNull(commentRepo);
            var genreRepo = _env.Provider.GetService(typeof(GenreRepo));
            Assert.NotNull(genreRepo);
            var bookGenreRepo = _env.Provider.GetService(typeof(BookGenreRepo));
            Assert.NotNull(bookGenreRepo);
            var publisherRepo = _env.Provider.GetService(typeof(PublisherRepo));
            Assert.NotNull(publisherRepo);
            var authorRepo = _env.Provider.GetService(typeof(AuthorRepo));
            Assert.NotNull(authorRepo);
            var bookInstanceRepo = _env.Provider.GetService(typeof(BookInstanceRepo));
            Assert.NotNull(bookInstanceRepo);
        }
        
        [Fact]
        public void UnitOfWorkInjectTest()
        {
            var bookService = _env.Provider.GetService(typeof(IBookService));
            Assert.NotNull(bookService);
            var issueService = _env.Provider.GetService(typeof(IIssueService));
            Assert.NotNull(issueService);
            var reserveService = _env.Provider.GetService(typeof(IReserveService));
            Assert.NotNull(reserveService);
            var searchBookService = _env.Provider.GetService(typeof(ISearchBookService));
            Assert.NotNull(searchBookService);
        }
    }
}