#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LibraryService.Domain.Core.Auth;
using LibraryService.Domain.Core.Extensions;
using LibraryService.Domain.Core.QueryTypes;
using LibraryService.Infrastructure.Data;
using LibraryService.Infrastructure.Data.NoKeyEntitySql;
using LibraryService.Infrastructure.Data.Repositories;
using LibraryService.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

#endregion

namespace LibraryService.Tests
{
    public class TestEnvironment
    {
        private IServiceCollection _services;
        public readonly ServiceProvider Provider;
        public IConfiguration Configuration;
        public TestEnvironment()
        {
            PrepareDependencyCollection();
            Provider = _services.BuildServiceProvider();
            SetConfiguration();
            InitDb().Wait();
            UpdateDbViews().Wait();
        }

        private void SetConfiguration()
        {
            Configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.Development.json")
                .AddEnvironmentVariables()
                .Build();
        }
        
        private void PrepareDependencyCollection()
        {
            _services = new ServiceCollection();
            _services.AddAutoMapper(typeof(AutoMapperProfile));
            _services.AddDbContext<LibraryDbContext>(options =>
                options.UseInMemoryDatabase(Guid.NewGuid().ToString()));

            _services.AddIdentity<User, Role>(config =>
                {
                    config.SignIn.RequireConfirmedEmail = false;
                    config.Password.RequireDigit = true;
                    config.Password.RequireLowercase = true;
                    config.Password.RequireUppercase = true;
                    config.Password.RequireNonAlphanumeric = false;
                    config.Password.RequiredLength = 8;
                })
                .AddEntityFrameworkStores<LibraryDbContext>()
                .AddTokenProvider<DataProtectorTokenProvider<User>>(TokenOptions.DefaultProvider);

            _services.AddRepositories();
            _services.AddUnitOfWork();
            _services.AddTransient<DataInit>();
            _services.AddLogging(logging => logging.AddConsole());
        }

        private async Task InitDb()
        {
            var initializer = (DataInit)Provider.GetService(typeof(DataInit));
            await initializer.Initilize();
        }

        // Обновление View для InMemory БД
        public async Task UpdateDbViews()
        {
            var db = Provider.GetService(typeof(LibraryDbContext)) as LibraryDbContext;
            if (db.IsInMemoryDb())
            {
                var bookRepo = Provider.GetService(typeof(BookRepo)) as BookRepo;
                var books = bookRepo?.GetListQuery()
                    .Include(p => p.BookInstances)
                    .Include(p => p.ReserveHistory)
                    .Include(p => p.IssueHistory)
                    .ToList();
                List<BookInStock> bookInStocks = new List<BookInStock>();
                if (db.BooksInStock.Any())
                {
                    bookInStocks = db.BooksInStock.ToList();
                    db.RemoveRange(bookInStocks);
                    await db.SaveChangesAsync();
                    bookInStocks = new List<BookInStock>();
                }
                
                foreach (var book in books)
                {
                    bookInStocks.Add(new BookInStock()
                    {
                        IdBook = book.Id,
                        InstancesCount = book.BookInstances.Count(),
                        ReservesCount = book.ReserveHistory.Count(p => p.IsActive && p.Date.AddDays(1) > DateTimeExtension.CurrentTimeLong()),
                        IssuesCount = book.IssueHistory.Count(p => !p.IsReturned)
                    });
                }

                await db.BooksInStock.AddRangeAsync(bookInStocks);
                await db.SaveChangesAsync();
            }
        }
        
        
    }
}