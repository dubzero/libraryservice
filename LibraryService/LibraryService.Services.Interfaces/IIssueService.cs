#region

using System.Threading.Tasks;
using LibraryService.Domain.Core;
using LibraryService.Domain.Core.Auth;

#endregion

namespace LibraryService.Services.Interfaces
{
    public interface IIssueService
    {
        Task MakeIssue(Book book, User user);
        Task MakeIssue(Reserve reserve);
        Task ReturnBook(Issue issued);
    }
}