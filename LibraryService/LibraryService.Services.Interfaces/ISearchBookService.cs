#region

using System.Collections.Generic;
using System.Threading.Tasks;
using LibraryService.Domain.Core;

#endregion

namespace LibraryService.Services.Interfaces
{
    public interface ISearchBookService
    {
        Task<IEnumerable<Book>> FindBooksAsync(int? idAuthor, int? idPublisher, IEnumerable<int> genres, int skip = 0,
            int take = 10);

        Task<int> BooksCount(int? idAuthor, int? idPublisher, IEnumerable<int> genres);
    }
}