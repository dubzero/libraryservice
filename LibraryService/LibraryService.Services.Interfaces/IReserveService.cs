﻿#region

using System.Threading.Tasks;
using LibraryService.Domain.Core;
using LibraryService.Domain.Core.Auth;

#endregion

namespace LibraryService.Services.Interfaces
{
    public interface IReserveService
    {
        Task MakeReserve(Book book, User user);
        Task CancelReserve(int idReserve);
    }
}