using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using LibraryService.Domain.Core.Auth;

namespace LibraryService.Services.Interfaces
{
    public interface IUserService
    {
        Task AddUser(User user, IEnumerable<string> roles, string password);
        Task UpdateUser(User user, IEnumerable<string> roles);
        Task RemoveUser(User user);
        Task ChangePassword(int idUser, string newPassword, string code);
    }
}