#region

using System.Threading.Tasks;
using LibraryService.Domain.Core;

#endregion

namespace LibraryService.Services.Interfaces
{
    public interface IBookService
    {
        Task CreateBook(Book book);
        Task AddBookInstance(Book book);
        Task<bool> DeleteBook(Book book);
    }
}