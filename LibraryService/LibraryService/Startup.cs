#region

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using AutoMapper;
using LibraryService.Domain.Core.Auth;
using LibraryService.Domain.Interfaces;
using LibraryService.Infrastructure.Data;
using LibraryService.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;

#endregion

namespace LibraryService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(AutoMapperProfile));
            var dbConnection = Configuration.GetConnectionString("LibraryDbConnection");
            string dbPassword = "672412Aa";
#if !DEBUG
            dbPassword = Environment.GetEnvironmentVariable("DATABASE_PASSWORD");
#endif
            dbConnection = dbConnection.Replace("DATABASE_PASSWORD", dbPassword);
            Console.WriteLine(dbConnection);
            services.AddDbContext<LibraryDbContext>(options =>
                options.UseNpgsql(dbConnection,
                    b =>
                    {
                        b.MigrationsAssembly("LibraryService.Infrastructure.Data");
                    }));

#if DEBUG
            services.AddDatabaseDeveloperPageExceptionFilter();
#endif

            services.AddRepositories();
            services.AddTransient<DataInit>();
            services.AddUnitOfWork();
            services.AddTransient<IEmailSender, EmailSender>();
            
            services.AddIdentity<User, Role>(config =>
                {
                    config.SignIn.RequireConfirmedEmail = false;
                    config.Password.RequireDigit = true;
                    config.Password.RequireLowercase = true;
                    config.Password.RequireUppercase = true;
                    config.Password.RequireNonAlphanumeric = false;
                    config.Password.RequiredLength = 8;
                })
                .AddEntityFrameworkStores<LibraryDbContext>()
                .AddTokenProvider<DataProtectorTokenProvider<User>>(TokenOptions.DefaultProvider);

            services.Configure<RequestLocalizationOptions>(
                opts =>
                {
                    var supportedCultures = new List<CultureInfo>
                    {
                        new CultureInfo("ru-RU")
                    };

                    opts.DefaultRequestCulture = new RequestCulture("ru-RU");
                    opts.SupportedCultures = supportedCultures;
                    opts.SupportedUICultures = supportedCultures;
                });

            var builder = services.AddControllersWithViews()
                .AddDataAnnotationsLocalization(options =>
                {
                    options.DataAnnotationLocalizerProvider = (type, factory) =>
                        factory.Create(typeof(SharedResource));
                })
                .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix);

#if DEBUG
            builder.AddRazorRuntimeCompilation();
#endif
        }


        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, DataInit init)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMigrationsEndPoint();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "files")),
                RequestPath = "/files"
            });
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "assets")),
                RequestPath = "/assets"
            });

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    "default",
                    "{controller=Dashboard}/{action=Index}/{id?}");
            });
            
            init.SetAssetsBasePath(env.WebRootPath);
            init.Initilize().Wait();
        }
    }
}