#region

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using LibraryService.Domain.Core;
using LibraryService.Domain.Core.Auth;
using LibraryService.Domain.Core.DataImport;
using LibraryService.Domain.Core.Extensions;
using LibraryService.Infrastructure.Data.Repositories;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Hosting.Internal;
using MoreLinq;

#endregion

namespace LibraryService
{
    public class DataInit
    {
        private readonly UsersRepo _usersRepo;
        private readonly RolesRepo _rolesRepo;
        private readonly GenreRepo _genreRepo;
        private readonly PublisherRepo _publisherRepo;
        private readonly AuthorRepo _authorRepo;
        private readonly BookRepo _bookRepo;
        private readonly BookInstanceRepo _bookInstanceRepo;
        private string _assetsBasePath;

        public DataInit(UsersRepo usersRepo, RolesRepo rolesRepo, GenreRepo genreRepo,
            PublisherRepo publisherRepo, AuthorRepo authorRepo, BookRepo bookRepo, BookInstanceRepo bookInstanceRepo)
        {
            _usersRepo = usersRepo;
            _rolesRepo = rolesRepo;
            _genreRepo = genreRepo;
            _publisherRepo = publisherRepo;
            _authorRepo = authorRepo;
            _bookRepo = bookRepo;
            _bookInstanceRepo = bookInstanceRepo;
            _assetsBasePath = Directory.GetCurrentDirectory();
        }

        public void SetAssetsBasePath(string path)
        {
            _assetsBasePath = path;
        }

        public async Task Initilize()
        {
            if (!_rolesRepo.GetListQuery().Any())
            {
                await AddRoles();
            }

            if (!_usersRepo.GetListQuery().Any())
            {
                await AddAdmin();
                await AddTestUsers();
            }

            if (!_genreRepo.GetListQuery().Any())
            {
                await ParseGenres();
            }

            if (!_bookRepo.GetListQuery().Any())
            {
                await ParseBooks();
            }
        }

        private async Task ParseGenres()
        {
            
            var relativePath = Path.Combine(_assetsBasePath, @"assets\genres.txt");
            var genresString = File.ReadLines(relativePath, Encoding.UTF8);
            List<Genre> genres = new List<Genre>();
            foreach (var genre in genresString)
            {
                genres.Add(new Genre(genre));
            }

            await _genreRepo.AddRangeAsync(genres);
        }

        private async Task AddRoles()
        {
            Role dev = new Role
            {
                Name = "Admin",
                Description = "Администратор"
            };

            Role librarian = new Role
            {
                Name = "Librarian",
                Description = "Библиотекарь"
            };

            Role reader = new Role
            {
                Name = "Reader",
                Description = "Читатель"
            };

            await _rolesRepo.AddAsync(dev);
            await _rolesRepo.AddAsync(librarian);
            await _rolesRepo.AddAsync(reader);
        }

        private async Task AddAdmin()
        {
            var user = new User
            {
                Email = "dev@mail.com",
                UserName = "dev@mail.com",
                FirstName = "Fn",
                LastName = "Ln",
                Patronymic = "Pc"
            };

            await _usersRepo.AddAsync(user, "672412Aa");
            await _usersRepo.GetAsync(user.Id);
            await _usersRepo.AddRoles(user, new[] {"Admin"});
        }

        private async Task AddTestUsers()
        {
            var librarian = new User
            {
                Email = "librarian@mail.com",
                UserName = "librarian@mail.com",
                FirstName = "Lib",
                LastName = "Ra",
                Patronymic = "Rian"
            };

            await _usersRepo.AddAsync(librarian, "Libra6724");
            await _usersRepo.AddRoles(librarian, new[] {"Librarian"});

            var reader = new User
            {
                Email = "reader@mail.com",
                UserName = "reader@mail.com",
                FirstName = "Re",
                LastName = "A",
                Patronymic = "Der"
            };

            await _usersRepo.AddAsync(reader, "Read6724");
            await _usersRepo.AddRoles(reader, new[] {"Reader"});
        }


        private async Task ParseBooks()
        {
            var relativePath = Path.Combine(_assetsBasePath, @"assets\books.json");
            var booksJson = await File.ReadAllTextAsync(relativePath, Encoding.UTF8);
            var booksData = JsonSerializer.Deserialize<List<BooksImport>>(booksJson);

            // Загрузить Издателей
            var publishers = booksData.Select(p => new Publisher
            {
                Name = p.Publisher
            }).DistinctBy(p => p.Name);
            await _publisherRepo.AddRangeAsync(publishers);
            var publishersList = _publisherRepo.GetListQuery().ToList();

            // Загрузить Авторов
            var authors = booksData.Select(p => new Author
            {
                FullName = p.Author
            }).DistinctBy(p => p.FullName);
            await _authorRepo.AddRangeAsync(authors);
            var authorsList = _authorRepo.GetListQuery().ToList();

            // Загрузить Книги
            var books = booksData.Select(p => new Book
            {
                Title = p.Title,
                SubTitle = p.SubTitle,
                Description = p.Description,
                IdAuthor = authorsList.FirstOrDefault(a => a.FullName == p.Author)?.Id,
                IdPublisher = publishersList.FirstOrDefault(a => a.Name == p.Publisher)?.Id,
                Pages = p.Pages,
                Published = p.Published.StringToUnixTime("yyyy-MM-dd"),
                ISBN = p.ISBN,
                ImageLink = p.ImageLink
            });
            await _bookRepo.AddRangeAsync(books);

            books = _bookRepo.GetListQuery().ToList();

            var booksInstances = books.Select(p => new BookInstance
            {
                IdBook = p.Id
            });
            await _bookInstanceRepo.AddRangeAsync(booksInstances);
        }


    }
}