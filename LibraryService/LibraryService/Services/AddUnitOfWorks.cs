#region

using LibraryService.Infrastructure.Business;
using LibraryService.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;

#endregion

namespace LibraryService.Services
{
    public static class AddUnitOfWorks
    {
        public static void AddUnitOfWork(this IServiceCollection services)
        {
            services
                .AddTransient<IUserService, UserService>()
                .AddTransient<IIssueService, IssueService>()
                .AddTransient<IReserveService, ReserveService>()
                .AddTransient<IBookService, BookService>()
                .AddTransient<ISearchBookService, SearchBookService>()
                .AddTransient<IIssueService, IssueService>();
        }
    }
}