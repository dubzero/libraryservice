﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using LibraryService.Domain.Core;
using LibraryService.Domain.Core.Auth;
using LibraryService.Domain.Core.Extensions;
using LibraryService.Models;
using LibraryService.Models.Users;

#endregion

namespace LibraryService.Services
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserEditViewModel>();
            CreateMap<UserEditViewModel, User>()
                .ForMember(p => p.UserName, opts => opts.MapFrom(source => source.Email));
            CreateMap<RegisterViewModel, User>()
                .ForMember(p => p.UserName, opts => opts.MapFrom(source => source.Email));
            CreateMap<User, RegisterViewModel>();

            CreateMap<User, UserViewModel>();

            CreateMap<Role, Role>().ForMember(p => p.ConcurrencyStamp, opts => opts.Ignore());

            CreateMap<ValueTuple<User, List<string>>, UserViewModel>()
                .ForMember(p => p.Roles, opts => opts.MapFrom(source => string.Join("; ", source.Item2)))
                .ForMember(p => p.FirstName, opt => opt.MapFrom(p => p.Item1.FirstName))
                .ForMember(p => p.LastName, opt => opt.MapFrom(p => p.Item1.LastName))
                .ForMember(p => p.Email, opt => opt.MapFrom(p => p.Item1.Email))
                .ForMember(p => p.Id, opt => opt.MapFrom(p => p.Item1.Id));

            CreateMap<User, UserWithRoles>()
                .ForMember(p => p.FirstName, opts => opts.MapFrom(source => source.FirstName))
                .ForMember(p => p.LastName, opts => opts.MapFrom(source => source.LastName))
                .ForMember(p => p.Email, opts => opts.MapFrom(source => source.Email));

            CreateMap<UserWithRoles, User>()
                .ForMember(p => p.FirstName, opts => opts.MapFrom(source => source.FirstName))
                .ForMember(p => p.LastName, opts => opts.MapFrom(source => source.LastName))
                .ForMember(p => p.Email, opts => opts.MapFrom(source => source.Email))
                .ForMember(p => p.UserName, opts => opts.MapFrom(source => source.Email));

            CreateMap<RegisterViewModelWithRoles, User>()
                .ForMember(p => p.FirstName, opts => opts.MapFrom(source => source.FirstName))
                .ForMember(p => p.LastName, opts => opts.MapFrom(source => source.LastName))
                .ForMember(p => p.Email, opts => opts.MapFrom(source => source.Email))
                .ForMember(p => p.UserName, opts => opts.MapFrom(source => source.Email));

            CreateMap<RoleViewModel, Role>();
            CreateMap<Role, RoleViewModel>();

            CreateMap<GenreIndexViewModel, Genre>();
            CreateMap<Genre, GenreIndexViewModel>();

            CreateMap<GenreEditModel, Genre>();
            CreateMap<Genre, GenreEditModel>();

            CreateMap<PublisherIndexViewModel, Publisher>();
            CreateMap<Publisher, PublisherIndexViewModel>();

            CreateMap<PublisherEditModel, Publisher>();
            CreateMap<Publisher, PublisherEditModel>();

            CreateMap<AuthorViewModel, Author>();
            CreateMap<Author, AuthorViewModel>();

            CreateMap<AuthorEditModel, Author>();
            CreateMap<Author, AuthorEditModel>();

            CreateMap<BookEditModel, Book>()
                .ForMember(p => p.Published,
                    opt =>
                        opt.MapFrom(source => source.Published.StringToUnixTime()))
                .ForMember(p => p.Genres, source => source.Ignore());
            CreateMap<Book, BookEditModel>()
                .ForMember(p => p.Published,
                    opt =>
                        opt.MapFrom(source => source.Published.UnixTimeToISO()))
                .ForMember(p => p.Genres, opt =>
                    opt.MapFrom(source => source.Genres.Select(p => p.Id)));

            CreateMap<Book, BookSearchResultModel>()
                .ConstructUsing(book => new BookSearchResultModel(book));
        }
    }
}