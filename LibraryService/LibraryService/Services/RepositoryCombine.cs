﻿#region

using LibraryService.Infrastructure.Data.Repositories;
using Microsoft.Extensions.DependencyInjection;

#endregion

namespace LibraryService.Services
{
    public static class RepositoryCombine
    {
        public static void AddRepositories(this IServiceCollection services)
        {
            services
                .AddTransient<UsersRepo>()
                .AddTransient<RolesRepo>()
                .AddTransient<IssueRepo>()
                .AddTransient<BookRateRepo>()
                .AddTransient<ReserveRepo>()
                .AddTransient<BookRepo>()
                .AddTransient<CommentRepo>()
                .AddTransient<GenreRepo>()
                .AddTransient<BookGenreRepo>()
                .AddTransient<PublisherRepo>()
                .AddTransient<AuthorRepo>()
                .AddTransient<BookInstanceRepo>();
        }
    }
}