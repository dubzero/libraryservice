#region

using System.ComponentModel.DataAnnotations;
using LibraryService.Domain.Core;
using LibraryService.Domain.Core.Extensions;

#endregion

namespace LibraryService.Models
{
    public class ReserveIndexViewModel : AbstractViewModel
    {
        public ReserveIndexViewModel()
        {
        }

        public ReserveIndexViewModel(Reserve model)
        {
            Id = model.Id;
            Date = model.Date.UnixTimeToString();
            Book = model.Book.Title;
            IsActive = model.IsActive;
        }

        [Display(Name = "Дата бронирования")] public string Date { get; set; }

        [Display(Name = "Книга")] public string Book { get; set; }

        [Display(Name = "Бронирование истекает")]
        public bool IsActive { get; set; }
    }

    public class ReserveForIssueViewModel : AbstractViewModel
    {
        public ReserveForIssueViewModel()
        {
        }

        public ReserveForIssueViewModel(Reserve model)
        {
            Id = model.Id;
            Date = model.Date.UnixTimeToString();
            Book = model.Book.Title;
            UserFullName = model.User.GetFullName;
            Email = model.User.Email;
            IsActive = model.IsActive;
        }

        [Display(Name = "Дата бронирования")] public string Date { get; set; }

        [Display(Name = "Книга")] public string Book { get; set; }
        [Display(Name = "Читатель")] public string UserFullName { get; set; }
        [Display(Name = "Email")] public string Email { get; set; }

        [Display(Name = "Бронирование истекает")]
        public bool IsActive { get; set; }
    }
}