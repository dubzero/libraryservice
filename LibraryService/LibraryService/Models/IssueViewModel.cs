#region

using System.ComponentModel.DataAnnotations;
using LibraryService.Domain.Core;
using LibraryService.Domain.Core.Extensions;

#endregion

namespace LibraryService.Models
{
    public class IssueIndexViewModel : AbstractViewModel
    {
        public IssueIndexViewModel()
        {
        }

        public IssueIndexViewModel(Issue model)
        {
            Id = model.Id;
            Date = model.Date.UnixTimeToString();
            Book = model.Book.Title;
            IsReturned = model.IsReturned;
        }

        [Display(Name = "Дата выдачи")] public string Date { get; set; }

        [Display(Name = "Книга")] public string Book { get; set; }

        public bool IsReturned { get; set; }
    }

    public class IssueReturnViewModel : AbstractViewModel
    {
        public IssueReturnViewModel()
        {
        }

        public IssueReturnViewModel(Issue model)
        {
            Id = model.Id;
            Date = model.Date.UnixTimeToString();
            Book = model.Book.Title;
            UserFullName = model.User.GetFullName;
            Email = model.User.Email;
            IsReturned = model.IsReturned;
        }

        [Display(Name = "Дата выдачи")] public string Date { get; set; }

        [Display(Name = "Книга")] public string Book { get; set; }
        [Display(Name = "Читатель")] public string UserFullName { get; set; }
        [Display(Name = "Email")] public string Email { get; set; }

        public bool IsReturned { get; set; }
    }
}