#region

using System.ComponentModel.DataAnnotations;

#endregion

namespace LibraryService.Models.Users
{
    public class ResetPasswordViewModel
    {
        public ResetPasswordViewModel()
        {
        }

        public ResetPasswordViewModel(int idUser, string code)
        {
            IdUser = idUser;
            Code = code;
        }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        public int IdUser { get; set; }

        [StringLength(100, ErrorMessage = "Пароль должен содержать более {2} символов", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароль и его подтверждение не совпадают")]
        [Display(Name = "Подтверждение пароля")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }
}