﻿namespace LibraryService.Models.Users
{
    public class LoginViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }

    public class LoginAPIViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}