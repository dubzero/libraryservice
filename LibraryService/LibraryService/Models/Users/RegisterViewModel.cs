﻿#region

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

#endregion

namespace LibraryService.Models.Users
{
    public class RegisterViewModel
    {
        [JsonProperty("firstName")]
        [Display(Name = "Имя")]
        [Required]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        [Display(Name = "Фамилия")]
        [Required]
        public string LastName { get; set; }

        [JsonProperty("patronymic")]
        [Display(Name = "Отчество")]
        public string Patronymic { get; set; } = "";

        [JsonProperty("email")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [JsonProperty("password")]
        [Display(Name = "Пароль")]
        public string Password { get; set; }
    }

    public class RegisterViewModelWithRoles : RegisterViewModel
    {
        [Display(Name = "Роли")]
        [JsonProperty("roles")]
        public IEnumerable<string> Roles { get; set; }
    }
}