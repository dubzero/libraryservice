﻿#region

using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

#endregion

namespace LibraryService.Models.Users
{
    public class RoleViewModel : AbstractViewModel
    {
        [Display(Name = "Код")]
        [JsonProperty("id")]
        public override int Id { get; set; }

        [Display(Name = "Роль")]
        [JsonProperty("name")]
        public string Name { get; set; }

        [Display(Name = "Описание")]
        [JsonProperty("description")]
        public string Description { get; set; }
    }
}