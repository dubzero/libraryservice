#region

using System.ComponentModel.DataAnnotations;
using LibraryService.Domain.Core;

#endregion

namespace LibraryService.Models
{
    public class BookSearchResultModel : AbstractViewModel
    {
        public BookSearchResultModel()
        {
        }

        public BookSearchResultModel(Book book)
        {
            Id = book.Id;
            Title = book.Title;
            Subtitle = book.SubTitle;
            Publisher = book.Publisher.Name ?? "";
            Author = book.Author.FullName ?? "";
            ImageLink = book.ImageLink;
        }

        [Display(Name = "Название")] public string Title { get; set; }
        [Display(Name = "Подзаголовок")] public string Subtitle { get; set; }
        [Display(Name = "Издатель")] public string Publisher { get; set; }
        [Display(Name = "Автор")] public string Author { get; set; }

        public int InStock { get; set; }

        public string ImageLink { get; set; } = "../assets/default-book.png";
    }
}