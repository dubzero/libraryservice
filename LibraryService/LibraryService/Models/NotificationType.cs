namespace LibraryService.Models
{
    public class Notification
    {
        public NotificationType Type { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
    }

    public class DownloadNotification : Notification
    {
        public string Url { get; set; }
    }

    public enum NotificationType
    {
        Success = 0,
        Error = 1,
        Notice = 2
    }
}