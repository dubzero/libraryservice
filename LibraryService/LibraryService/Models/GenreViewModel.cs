#region

using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

#endregion

namespace LibraryService.Models
{
    public class GenreIndexViewModel : AbstractViewModel
    {
        [Display(Name = "Жанр")]
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class GenreEditModel : AbstractViewModel
    {
        [Display(Name = "Жанр")]
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}