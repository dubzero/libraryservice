#region

using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

#endregion

namespace LibraryService.Models
{
    public class AuthorViewModel : AbstractViewModel
    {
        [Display(Name = "ФИО")]
        [JsonProperty("fullName")]
        public string FullName { get; set; }
    }

    public class AuthorEditModel : AbstractViewModel
    {
        [Display(Name = "ФИО")]
        [JsonProperty("fullName")]
        public string FullName { get; set; }
    }
}