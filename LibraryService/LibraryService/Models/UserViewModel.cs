﻿#region

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

#endregion

namespace LibraryService.Models
{
    public class UserEditViewModel : AbstractViewModel
    {
        [Display(Name = "Код")]
        [JsonProperty("id")]
        public override int Id { get; set; }

        [Display(Name = "Email")]
        [JsonProperty("email")]
        public string Email { get; set; }

        [Display(Name = "Имя")]
        [StringLength(100)]
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")]
        [StringLength(100)]
        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [Display(Name = "Отчество")]
        [StringLength(50)]
        [JsonProperty("patronymic")]
        public string Patronymic { get; set; }
    }

    public class UserWithRoles : UserEditViewModel
    {
        [Display(Name = "Роли")]
        [JsonProperty("roles")]
        public IEnumerable<string> Roles { get; set; } = new List<string>();
    }


    public class UserViewModel : AbstractViewModel
    {
        [Display(Name = "Код")]
        [JsonProperty("id")]
        public override int Id { get; set; }

        [Display(Name = "Email")]
        [JsonProperty("email")]
        public string Email { get; set; }

        [Display(Name = "Имя")]
        [StringLength(50)]
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")]
        [StringLength(50)]
        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [Display(Name = "Отчество")]
        [StringLength(50)]
        [JsonProperty("patronymic")]
        public string Patronymic { get; set; }

        [Display(Name = "Роли")]
        [StringLength(200)]
        [JsonProperty("roles")]
        public string Roles { get; set; }

        // Иванов Александр Александрович
        [Display(Name = "Полное имя")] public string GetFullName => $"{LastName} {FirstName} {Patronymic}";

        // Иванов А.А.
        public string GetShortFullName => $"{LastName} {FirstName.Substring(0, 1).ToUpper()}." +
                                          $"{Patronymic.Substring(0, 1).ToUpper()}.";
    }
}