#region

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using LibraryService.Domain.Core.Extensions;

#endregion

namespace LibraryService.Models
{
    public class BookIndexViewModel : AbstractViewModel
    {
        public BookIndexViewModel()
        {
        }

        public BookIndexViewModel(int id, string title, long? published, string publisher, string author,
            IEnumerable<string> genres)
        {
            Id = id;
            Title = title;
            Published = published.UnixTimeToString();
            Publisher = publisher;
            Author = author;
            Genres = string.Join(';', genres);
        }


        [Display(Name = "Название")] public string Title { get; set; }
        [Display(Name = "Опубликовано")] public string Published { get; set; }
        [Display(Name = "Издатель")] public string Publisher { get; set; }
        [Display(Name = "Автор")] public string Author { get; set; }
        [Display(Name = "Жанры")] public string Genres { get; set; }
    }

    public class BookEditModel : AbstractViewModel
    {
        [Required]
        [Display(Name = "Название")]
        public string Title { get; set; }

        [Display(Name = "Подзаголовок ")] public string SubTitle { get; set; }

        [Display(Name = "Описание")] public string Description { get; set; }

        [Display(Name = "Дата публикации")] public string Published { get; set; }

        [Display(Name = "Кол-во. страниц")] public int? Pages { get; set; }

        [Display(Name = "ISBN")] public string ISBN { get; set; }

        [Display(Name = "Издатель")] public int? IdPublisher { get; set; }

        [Display(Name = "Автор")] public int? IdAuthor { get; set; }

        [Display(Name = "Жанры")] public IEnumerable<int> Genres { get; set; }
    }
}