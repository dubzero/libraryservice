﻿#region

using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

#endregion

namespace LibraryService.Models
{
    public class AbstractViewModel
    {
        [Display(Name = "Код")]
        [JsonProperty("id")]
        public virtual int Id { get; set; }
    }
}