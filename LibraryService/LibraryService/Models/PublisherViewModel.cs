#region

using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

#endregion

namespace LibraryService.Models
{
    public class PublisherIndexViewModel : AbstractViewModel
    {
        [Display(Name = "Издатель")]
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class PublisherEditModel : AbstractViewModel
    {
        [Display(Name = "Издатель")]
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}