#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LibraryService.Domain.Core;
using LibraryService.Domain.Core.Extensions;
using LibraryService.Infrastructure.Data.Repositories;
using LibraryService.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

#endregion

namespace LibraryService.Controllers
{
    [Authorize(Roles = ClaimsPrincipalExtension.AdminLevelString)]
    public class AuthorsController : Controller
    {
        private readonly AuthorRepo _authorRepo;
        private readonly IMapper _mapper;
        private readonly ILogger<Author> _logger;

        public AuthorsController(AuthorRepo authorRepo, IMapper mapper, ILogger<Author> logger)
        {
            _authorRepo = authorRepo;
            _mapper = mapper;
            _logger = logger;
        }

        public IActionResult Index()
        {
            var authorViewList = new List<AuthorViewModel>();
            var authors = _authorRepo.GetListQuery().ToList();
            foreach (var author in authors)
            {
                var authorViewModel = _mapper.Map<AuthorViewModel>(author);

                authorViewList.Add(authorViewModel);
            }

            return View(authorViewList);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AuthorEditModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var entity = _mapper.Map<Author>(model);

                    if (await _authorRepo.IsDuplicate(entity))
                    {
                        ModelState.AddModelError(nameof(Author.FullName),
                            "Автор с такими характеристиками уже существует в базе");
                        throw new Exception($"{nameof(model.GetType)} duplicated record");
                    }

                    await _authorRepo.AddAsync(entity);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{nameof(Create)} {model.GetType()} exception: {ex.Message}");
                }
            }

            return View(_mapper.Map<AuthorEditModel>(model));
        }

        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                _logger.LogError($"{nameof(Edit)} entity {typeof(Author)} not found");
                return NotFound();
            }

            var entity = _authorRepo.Get(id.Value);
            if (entity == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<AuthorEditModel>(entity));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(AuthorEditModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var entity = _mapper.Map<Author>(model);

                    if (await _authorRepo.IsDuplicate(entity))
                    {
                        ModelState.AddModelError(nameof(Author.FullName),
                            "Автор с такими характеристиками уже существует в базе");
                        throw new Exception($"{nameof(model.GetType)} duplicated record");
                    }

                    await _authorRepo.UpdateAsync(entity);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{nameof(Create)} {model.GetType()} exception: {ex.Message}");
                }
            }

            return View(_mapper.Map<AuthorEditModel>(model));
        }

        public virtual IActionResult Delete([FromRoute] int? id)
        {
            if (id == null) return NotFound();

            var entity = _authorRepo.Get(id.Value);
            if (entity == null) return NotFound();

            var model = _mapper.Map<AuthorViewModel>(entity);

            return View("Delete", model);
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var entity = await _authorRepo.GetAsync(id);
                var result = _authorRepo.Delete(entity);

                return result ? RedirectToAction("Index") : RedirectToAction("Delete", id);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Delete: {ex.Message}");
                return RedirectToAction("Delete", id);
            }
        }
    }
}