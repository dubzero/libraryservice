#region

using System.Linq;
using System.Threading.Tasks;
using LibraryService.Domain.Core.Extensions;
using LibraryService.Infrastructure.Data.Repositories;
using LibraryService.Models;
using LibraryService.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

#endregion

namespace LibraryService.Controllers
{
    public class IssueController : Controller
    {
        private readonly ReserveRepo _reserveRepo;
        private readonly IIssueService _issueBook;
        private readonly UsersRepo _usersRepo;
        private readonly IssueRepo _issueRepo;

        public IssueController(ReserveRepo reserveRepo, IIssueService issueBook, UsersRepo usersRepo,
            IssueRepo issueRepo)
        {
            _reserveRepo = reserveRepo;
            _issueBook = issueBook;
            _usersRepo = usersRepo;
            _issueRepo = issueRepo;
        }

        [Authorize(Roles = "Reader")]
        public async Task<IActionResult> Index()
        {
            var user = await _usersRepo.GetCurrentUser(User);
            var issues = await _issueRepo.GetIssuedBooks(user.Id).ToListAsync();

            var models = issues.Select(p => new IssueIndexViewModel(p));

            return View(models);
        }

        [HttpGet]
        [Authorize(Roles = ClaimsPrincipalExtension.LibrarianLevelString)]
        public async Task<IActionResult> ReservedBooks()
        {
            var reservedBooks = await _reserveRepo.GetReservedBooks(null)
                .Include(p => p.Book)
                .Include(p => p.User).ToListAsync();
            var models = reservedBooks.Select(p => new ReserveForIssueViewModel(p));
            return View(models);
        }

        [Authorize(Roles = ClaimsPrincipalExtension.LibrarianLevelString)]
        public async Task<IActionResult> MakeBookIssue(int idReserve)
        {
            var reserve = await _reserveRepo.GetAsync(idReserve);
            await _issueBook.MakeIssue(reserve);

            return RedirectToAction("ReservedBooks");
        }

        [HttpGet]
        [Authorize(Roles = ClaimsPrincipalExtension.LibrarianLevelString)]
        public async Task<IActionResult> IssuedBooks()
        {
            var issuedBooks = await _issueRepo.GetIssuedBooks(null)
                .Include(p => p.Book)
                .Include(p => p.User).ToListAsync();

            var models = issuedBooks.Select(p => new IssueReturnViewModel(p));
            return View(models);
        }

        [Authorize(Roles = ClaimsPrincipalExtension.LibrarianLevelString)]
        public async Task<IActionResult> ReturnBook(int idIssue)
        {
            var issue = await _issueRepo.GetAsync(idIssue);
            await _issueBook.ReturnBook(issue);
            return RedirectToAction("IssuedBooks");
        }
    }
}