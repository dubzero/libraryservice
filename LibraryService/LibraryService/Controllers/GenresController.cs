#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LibraryService.Domain.Core;
using LibraryService.Domain.Core.Extensions;
using LibraryService.Infrastructure.Data.Repositories;
using LibraryService.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

#endregion

namespace LibraryService.Controllers
{
    [Authorize(Roles = ClaimsPrincipalExtension.AdminLevelString)]
    public class GenresController : Controller
    {
        private readonly GenreRepo _genreRepo;
        private readonly IMapper _mapper;
        private readonly ILogger<Genre> _logger;

        public GenresController(GenreRepo genreRepo, IMapper mapper, ILogger<Genre> logger)
        {
            _genreRepo = genreRepo;
            _mapper = mapper;
            _logger = logger;
        }

        public IActionResult Index()
        {
            var genreViewList = new List<GenreIndexViewModel>();
            var genres = _genreRepo.GetListQuery().ToList();
            foreach (var genre in genres)
            {
                var genreViewModel = _mapper.Map<GenreIndexViewModel>(genre);

                genreViewList.Add(genreViewModel);
            }

            return View(genreViewList);
        }


        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GenreEditModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var entity = _mapper.Map<Genre>(model);

                    if (await _genreRepo.IsDuplicate(entity))
                    {
                        ModelState.AddModelError(nameof(Genre.Name),
                            "Жанр с такими характеристиками уже существует в базе");
                        throw new Exception($"{nameof(model.GetType)} duplicated record");
                    }

                    await _genreRepo.AddAsync(entity);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{nameof(Create)} {model.GetType()} exception: {ex.Message}");
                }
            }

            return View(_mapper.Map<GenreEditModel>(model));
        }

        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                _logger.LogError($"{nameof(Edit)} entity {typeof(Genre)} not found");
                return NotFound();
            }

            var entity = _genreRepo.Get(id.Value);
            if (entity == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GenreEditModel>(entity));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GenreEditModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var entity = _mapper.Map<Genre>(model);

                    if (await _genreRepo.IsDuplicate(entity))
                    {
                        ModelState.AddModelError(nameof(Genre.Name),
                            "Жанр с такими характеристиками уже существует в базе");
                        throw new Exception($"{nameof(model.GetType)} duplicated record");
                    }

                    await _genreRepo.UpdateAsync(entity);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{nameof(Create)} {model.GetType()} exception: {ex.Message}");
                }
            }

            return View(_mapper.Map<GenreEditModel>(model));
        }

        public virtual IActionResult Delete([FromRoute] int? id)
        {
            if (id == null) return NotFound();

            var entity = _genreRepo.Get(id.Value);
            if (entity == null) return NotFound();

            var model = _mapper.Map<GenreIndexViewModel>(entity);

            return View("Delete", model);
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var entity = await _genreRepo.GetAsync(id);
                var result = _genreRepo.Delete(entity);

                return result ? RedirectToAction("Index") : RedirectToAction("Delete", id);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Delete: {ex.Message}");
                return RedirectToAction("Delete", id);
            }
        }
    }
}