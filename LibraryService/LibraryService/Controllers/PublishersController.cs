#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LibraryService.Domain.Core;
using LibraryService.Domain.Core.Extensions;
using LibraryService.Infrastructure.Data.Repositories;
using LibraryService.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

#endregion

namespace LibraryService.Controllers
{
    [Authorize(Roles = ClaimsPrincipalExtension.AdminLevelString)]
    public class PublishersController : Controller
    {
        private readonly PublisherRepo _publisherRepo;
        private readonly IMapper _mapper;
        private readonly ILogger<Publisher> _logger;

        public PublishersController(IMapper mapper, PublisherRepo publisherRepo, ILogger<Publisher> logger)
        {
            _mapper = mapper;
            _publisherRepo = publisherRepo;
            _logger = logger;
        }

        public IActionResult Index()
        {
            var publisherViewList = new List<PublisherIndexViewModel>();
            var publishers = _publisherRepo.GetListQuery().ToList();
            foreach (var publisher in publishers)
            {
                var publisherViewModel = _mapper.Map<PublisherIndexViewModel>(publisher);

                publisherViewList.Add(publisherViewModel);
            }

            return View(publisherViewList);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PublisherEditModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var entity = _mapper.Map<Publisher>(model);

                    if (await _publisherRepo.IsDuplicate(entity))
                    {
                        ModelState.AddModelError(nameof(Publisher.Name),
                            "Издатель с такими характеристиками уже существует в базе");
                        throw new Exception($"{nameof(model.GetType)} duplicated record");
                    }

                    await _publisherRepo.AddAsync(entity);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{nameof(Create)} {model.GetType()} exception: {ex.Message}");
                }
            }

            return View(_mapper.Map<PublisherEditModel>(model));
        }

        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                _logger.LogError($"{nameof(Edit)} entity {typeof(Publisher)} not found");
                return NotFound();
            }

            var entity = _publisherRepo.Get(id.Value);
            if (entity == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<PublisherEditModel>(entity));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(PublisherEditModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var entity = _mapper.Map<Publisher>(model);

                    if (await _publisherRepo.IsDuplicate(entity))
                    {
                        ModelState.AddModelError(nameof(Publisher.Name),
                            "Издатель с такими характеристиками уже существует в базе");
                        throw new Exception($"{nameof(model.GetType)} duplicated record");
                    }

                    await _publisherRepo.UpdateAsync(entity);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{nameof(Create)} {model.GetType()} exception: {ex.Message}");
                }
            }

            return View(_mapper.Map<PublisherEditModel>(model));
        }

        public virtual IActionResult Delete([FromRoute] int? id)
        {
            if (id == null) return NotFound();

            var entity = _publisherRepo.Get(id.Value);
            if (entity == null) return NotFound();

            var model = _mapper.Map<PublisherIndexViewModel>(entity);

            return View("Delete", model);
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var entity = await _publisherRepo.GetAsync(id);
                var result = _publisherRepo.Delete(entity);

                return result ? RedirectToAction("Index") : RedirectToAction("Delete", id);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Delete: {ex.Message}");
                return RedirectToAction("Delete", id);
            }
        }
    }
}