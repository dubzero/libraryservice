﻿#region

using System;
using LibraryService.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

#endregion

namespace LibraryService.Controllers
{
    public abstract class BaseMvcController : Controller
    {
        public virtual IActionResult SuccesNotification(string message)
        {
            return PartialView("_Notification",
                new Notification
                {
                    Type = NotificationType.Success,
                    Title = "Успешно!",
                    Message = message
                });
        }

        public virtual IActionResult RowAddedNotification()
        {
            return PartialView("_Notification",
                new Notification
                {
                    Type = NotificationType.Success,
                    Title = "Успешно!",
                    Message = "Запись добавлена."
                });
        }


        public virtual IActionResult DefaultErrorNotification()
        {
            return PartialView("_Notification",
                new Notification
                {
                    Type = NotificationType.Error,
                    Title = "Ошибка!",
                    Message = "Причина ошибки неизвестна"
                });
        }

        public virtual IActionResult DefaultErrorNotification(Exception ex)
        {
            return PartialView("_Notification",
                new Notification
                {
                    Type = NotificationType.Error,
                    Title = "Ошибка!",
                    Message = ex.Message
                });
        }


        public virtual IActionResult RowEditedNotification()
        {
            return PartialView("_Notification",
                new Notification
                {
                    Type = NotificationType.Success,
                    Title = "Успешно!",
                    Message = "Запись изменена"
                });
        }

        public virtual IActionResult RowDeletedNotification()
        {
            return PartialView("_Notification",
                new Notification
                {
                    Type = NotificationType.Success,
                    Title = "Успешно!",
                    Message = "Запись удалена"
                });
        }

        public virtual IActionResult InvalidModelState(ModelStateDictionary modelState)
        {
            var message = "";
            foreach (var error in modelState.Keys) message = $"{error}\n";

            return PartialView("_Notification",
                new Notification
                {
                    Type = NotificationType.Error,
                    Title = "Ошибка!",
                    Message = message
                });
        }
    }
}