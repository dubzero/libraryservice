#region

using System.Threading.Tasks;
using LibraryService.Domain.Core.Auth;
using LibraryService.Infrastructure.Data.Repositories;
using LibraryService.Models.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

#endregion

namespace LibraryService.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly UsersRepo _usersRepository;
        private readonly SignInManager<User> _signInManager;

        public AccountController(SignInManager<User> signInManager, UsersRepo userRepository)
        {
            _signInManager = signInManager;
            _usersRepository = userRepository;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync().ConfigureAwait(true);
            return RedirectToAction(nameof(Login), "Account");
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            await _signInManager.SignOutAsync().ConfigureAwait(true);

            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var user = await _usersRepository.GetListQuery().FirstOrDefaultAsync(p => p.Email == model.Email);
                if (user == null)
                {
                    ModelState.AddModelError(string.Empty, "Электронный адрес или пароль указан неверно.");
                    return View(model);
                }

                var result = await _signInManager
                    .PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false)
                    .ConfigureAwait(true);
                if (result.Succeeded)
                {
                    return RedirectToLocal(returnUrl);
                }

                ModelState.AddModelError(string.Empty, "Электронный адрес или пароль указан неверно.");
                return View(model);
            }

            return View(model);
        }


        [AllowAnonymous]
        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction(nameof(DashboardController.Index), "Dashboard");
        }
    }
}