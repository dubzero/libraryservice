#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryService.Infrastructure.Data.Repositories;
using LibraryService.Models;
using LibraryService.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

#endregion

namespace LibraryService.Controllers
{
    public class BookSearchController : Controller
    {
        private readonly ISearchBookService _searchBook;
        private readonly BookInstanceRepo _bookInstanceRepo;

        public BookSearchController(ISearchBookService searchBook, BookInstanceRepo bookInstanceRepo)
        {
            _searchBook = searchBook;
            _bookInstanceRepo = bookInstanceRepo;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> FindBooks(int? idAuthor, int? idPublisher, IEnumerable<int> genres,
            int skip = 0, int take = 10)
        {
            var books = await _searchBook.FindBooksAsync(idAuthor, idPublisher, genres, skip, take);
            var booksCount = await _searchBook.BooksCount(idAuthor, idPublisher, genres);

            var model = books.Select(p => new BookSearchResultModel(p)).ToList();
            var counts = books.Join(_bookInstanceRepo.GetBooksInStock(),
                    b => b.Id,
                    stock => stock.IdBook,
                    (b, stock) => new
                        {stock.IdBook, InStockCount = stock.InstancesCount - stock.ReservesCount - stock.IssuesCount})
                .ToList();


            foreach (var book in model)
            {
                book.InStock = counts.FirstOrDefault(p => book != null && p.IdBook == book.Id).InStockCount;
            }

            ViewBag.BooksCount = booksCount;
            ViewBag.PagesCount = Math.Ceiling(booksCount / (double) take);
            ViewBag.SelectedPage = skip + 1;
            return PartialView("_SearchResult", model);
        }
    }
}