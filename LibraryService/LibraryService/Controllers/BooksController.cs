using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LibraryService.Domain.Core;
using LibraryService.Domain.Core.Extensions;
using LibraryService.Infrastructure.Data.Repositories;
using LibraryService.Models;
using LibraryService.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace LibraryService.Controllers
{
    [Authorize(Roles = ClaimsPrincipalExtension.LibrarianLevelString)]
    public class BooksController : BaseMvcController
    {
        private readonly BookRepo _bookRepo;
        private readonly BookGenreRepo _bookGenreRepo;
        private readonly IMapper _mapper;
        private readonly ILogger<Book> _logger;
        private readonly IBookService _bookService;

        public BooksController(IMapper mapper, ILogger<Book> logger, BookRepo bookRepo, BookGenreRepo bookGenreRepo,
            IBookService bookService)
        {
            _mapper = mapper;
            _logger = logger;
            _bookRepo = bookRepo;
            _bookGenreRepo = bookGenreRepo;
            _bookService = bookService;
        }

        public virtual async Task<IActionResult> Index()
        {
            var books = await _bookRepo.GetListQuery().Include(p => p.Publisher)
                .Include(p => p.Author)
                .Include(p => p.Genres)
                .Select(p => new BookIndexViewModel(p.Id, p.Title, p.Published, p.Publisher.Name, p.Author.FullName,
                    p.Genres.Select(g => g.Name)))
                .ToListAsync();

            return View(books);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(BookEditModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var entity = _mapper.Map<Book>(model);

                    if (await _bookRepo.IsDuplicate(entity))
                    {
                        ModelState.AddModelError(nameof(Book.Title),
                            "Книга с такими характеристиками уже существует в базе");
                        throw new Exception($"{nameof(model.GetType)} duplicated record");
                    }

                    await _bookService.CreateBook(entity);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{nameof(Create)} {model.GetType()} exception: {ex.Message}");
                }
            }

            return View(_mapper.Map<BookEditModel>(model));
        }

        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                _logger.LogError($"{nameof(Edit)} entity {typeof(Book)} not found");
                return NotFound();
            }

            var entity = _bookRepo.GetListQuery().Include(p => p.Genres)
                .FirstOrDefault(p => p.Id == id.Value);
            if (entity == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<BookEditModel>(entity));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(BookEditModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var entity = _mapper.Map<Book>(model);

                    if (await _bookRepo.IsDuplicate(entity))
                    {
                        ModelState.AddModelError(nameof(Book.Title),
                            "Книга с такими характеристиками уже существует в базе");
                        throw new Exception($"{nameof(model.GetType)} duplicated record");
                    }

                    await _bookRepo.UpdateAsync(entity);
                    // Обновление жанров
                    await _bookGenreRepo.UpdateBookGeneres(entity, model.Genres);

                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{nameof(Create)} {model.GetType()} exception: {ex.Message}");
                }
            }

            return View(_mapper.Map<BookEditModel>(model));
        }

        public virtual IActionResult Delete([FromRoute] int? id)
        {
            if (id == null) return NotFound();

            var entity = _bookRepo.GetListQuery()
                .Include(p => p.Publisher)
                .Include(p => p.Author)
                .FirstOrDefault(p => p.Id == id.Value);
            if (entity == null) return NotFound();

            return View("Delete",
                new BookIndexViewModel(entity.Id, entity.Title, entity.Published, entity.Publisher.Name,
                    entity.Author.FullName, Array.Empty<string>()));
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var entity = await _bookRepo.GetAsync(id);
                var result = await _bookService.DeleteBook(entity);

                return result ? RedirectToAction("Index") : RedirectToAction("Delete", id);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Delete: {ex.Message}");
                return RedirectToAction("Delete", id);
            }
        }
    }
}