#region

using System.Linq;
using System.Threading.Tasks;
using LibraryService.Domain.Core.Extensions;
using LibraryService.Infrastructure.Data.Repositories;
using LibraryService.Models;
using LibraryService.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

#endregion

namespace LibraryService.Controllers
{
    [Authorize(Roles = ClaimsPrincipalExtension.ReaderLevelString)]
    public class ReserveController : Controller
    {
        private readonly ReserveRepo _reserveRepo;
        private readonly IReserveService _reserveBook;
        private readonly UsersRepo _usersRepo;
        private readonly BookRepo _bookRepo;

        public ReserveController(ReserveRepo reserveRepo, IReserveService reserveBook, UsersRepo usersRepo,
            BookRepo bookRepo)
        {
            _reserveRepo = reserveRepo;
            _reserveBook = reserveBook;
            _usersRepo = usersRepo;
            _bookRepo = bookRepo;
        }

        public async Task<IActionResult> Index()
        {
            var user = await _usersRepo.GetCurrentUser(User);
            var reserves = await _reserveRepo.GetReservedBooks(user.Id).ToListAsync();

            var models = reserves.Select(p => new ReserveIndexViewModel(p));

            return View(models);
        }

        [Authorize(Roles = "Reader")]
        public async Task<IActionResult> MakeReserve(int idBook)
        {
            var user = await _usersRepo.GetCurrentUser(User);
            var book = await _bookRepo.GetAsync(idBook);
            await _reserveBook.MakeReserve(book, user);
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Reader")]
        public async Task<IActionResult> CancelReserve(int idReserve)
        {
            await _reserveBook.CancelReserve(idReserve);
            return RedirectToAction("Index");
        }
    }
}