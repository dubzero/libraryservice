﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LibraryService.Domain.Core;
using LibraryService.Domain.Core.Auth;
using LibraryService.Domain.Core.Extensions;
using LibraryService.Infrastructure.Data.Repositories;
using LibraryService.Models;
using LibraryService.Models.Users;
using LibraryService.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

#endregion

namespace LibraryService.Controllers
{
    [Authorize(Roles = ClaimsPrincipalExtension.AdminLevelString)]
    public class UsersController : Controller
    {
        private readonly UsersRepo _usersRepo;
        private readonly RolesRepo _rolesRepo;
        private readonly IMapper _mapper;
        private readonly IStringLocalizer<SharedResource> _sharedLocalizer;
        private readonly ILogger<User> _logger;
        private readonly UserManager<User> _userManager;
        private readonly IUserService _userService;

        public UsersController(UsersRepo usersRepo, RolesRepo rolesRepository, IMapper mapper,
            IStringLocalizer<SharedResource> sharedLocalizer, ILogger<User> logger, UserManager<User> userManager, IUserService userService)
        {
            _usersRepo = usersRepo;
            _rolesRepo = rolesRepository;
            _mapper = mapper;
            _sharedLocalizer = sharedLocalizer;
            _logger = logger;
            _userManager = userManager;
            _userService = userService;
        }


        public async Task<IActionResult> Index()
        {
            var userViewList = new List<UserViewModel>();
            var currentUser = await _usersRepo.GetCurrentUser(User);
            var users = _usersRepo.GetListQuery().Where(p => p.Id != currentUser.Id).ToList();

            foreach (var user in users)
            {
                var userRoles = await _usersRepo.GetUserRoles(user);
                var rolesDescription = _rolesRepo.GetRolesDescription(userRoles).ToList();
                var userViewModel = _mapper.Map<UserViewModel>((user, rolesDescription));

                userViewList.Add(userViewModel);
            }

            return View(userViewList);
        }

        public virtual IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModelWithRoles entity)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = _mapper.Map<User>(entity);
                    var errors = ValidateUser(user);
                    if (!errors.Any())
                    {
                        await _userService.AddUser(user, entity.Roles, entity.Password);
                        return RedirectToAction("Index");
                    }

                    throw new Exception(string.Join("\n", errors.Select(p => p.Message)));
                }
                catch (Exception ex)
                {
                    return View(entity);
                }
            }

            return View(entity);
        }

        [HttpGet]
        public virtual async Task<IActionResult> EditWithRoles(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var entity = await _usersRepo.GetAsync(id.Value);
            if (entity == null)
            {
                return NotFound();
            }

            var userRoles = await _usersRepo.GetUserRoles(entity);

            var userWithRoles = _mapper.Map<UserWithRoles>(entity);
            userWithRoles.Roles = userRoles;

            return View(userWithRoles);
        }

        [HttpPost]
        public async Task<IActionResult> EditWithRoles(int id, UserWithRoles model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }

            if (!ModelState.IsValid) return View(model);
            
            var entity = _usersRepo.GetListQuery().FirstOrDefault(p => p.Id == id);
            if (entity == null)
            {
                return NotFound();
            }

            var user = _mapper.Map(model, entity);
            user.Id = id;
            var errors = ValidateUser(user);
            if (errors.Any())
            {
                return BadRequest(errors);
            }

            try
            {
                await _userService.UpdateUser(user, model.Roles);
            }
            catch (Exception ex)
            {
                errors.Add(new ErrorInfo(ex.Message));
                return BadRequest(errors);
            }

            return RedirectToAction("Index");

        }

        [HttpGet]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var entity = await _usersRepo.GetAsync(id.Value);
            if (entity == null)
            {
                _logger.LogError($"{nameof(Delete)} entity {typeof(User)} not found");
                return NotFound();
            }

            return View(_mapper.Map<UserViewModel>(entity));
        }

        // POST: CropsExample/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual async Task<IActionResult> DeleteConfirmed(int id)
        {
            var entity = await _usersRepo.GetAsync(id);
            await _userService.RemoveUser(entity);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> ChangePassword(int idUser)
        {
            var user = await _usersRepo.GetAsync(idUser);
            if (user == null)
            {
                return View("Index");
            }

            var code = await _userManager.GeneratePasswordResetTokenAsync(user);
            var model = new ResetPasswordViewModel(idUser, code)
            {
                Email = user.Email
            };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid) return View(model);
            try
            {
                await _userService.ChangePassword(model.IdUser, model.Password, model.Code);
            }
            catch
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }

        public virtual List<ErrorInfo> ValidateUser(User entity)
        {
            var res = ErrorList.GetErrors(entity);
            if (!_usersRepo.IsUnique(entity)) res.Add(new ErrorInfo(_sharedLocalizer["EntityExists"]));
            return res;
        }

        private bool EntityExists(int id)
        {
            return _usersRepo.GetListQuery().Any(e => e.Id == id);
        }
    }
}