# Getting started

### Внимание! 

По-умолчанию в строке подключения к базе указан 5433 порт, вместо 5432.

### appsettings.Development.json

```
"ConnectionStrings": {
    "LibraryDbConnection": "Host=localhost;Port=5433;Database=library_db;Username=postgres;Password=*******"
  },
```

### Миграции БД с помощью Dotnet CLI

```
cd LibraryService.Infrastructure.Data
dotnet ef --startup-project ../LibraryService/ migrations add YourMigration


cd ../
dotnet ef database update --project LibraryService
```

### Загрузка npm пакетов

```
cd LibraryService
сd wwwroot
npm install
```

## Тествые данные по-умолчанию:

### Аккаунт Администратора

email: dev@mail.com
pass: 672412Aa

### Аккаунт Библиотекаря

email: librarian@mail.com
pass: Libra6724

### Аккаунт Читателя

email: reader@mail.com
pass: Read6724

###  Запуск Staging профиля

Неопходимо переопределить переменную окружения ```DATABASE_PASSWORD``` для подключения к БД.

